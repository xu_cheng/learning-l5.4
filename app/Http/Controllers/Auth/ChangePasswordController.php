<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;


class ChangePasswordController extends Controller
{

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    //protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Change password view
     */
    public function changePasswordView()
    {
        return view('auth.passwords.change');
    }

    /**
     * Change user password
     */
    public function changePassword (Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'current_password' => 'required|min:6',
            'password' => 'required|confirmed|min:6',
        ]);

        $user = Auth::user();
        if (Hash::check($request->current_password, $user->password)) {
            // The passwords match...
            $user->password = Hash::make($request->password);
            $user->save();
            return back()->with('msg', 'Your new password updated successfully!');
        } else {
            return back()->withErrors('Your current password is incorrect!');
        }
    }
}
