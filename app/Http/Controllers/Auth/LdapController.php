<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Adldap\AdldapInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LdapController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/course/index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AdldapInterface $ldap, Request $request)
    {
        $this->ldap = $ldap;
        $this->middleware('guest')->except('logout');

        if ($request->method() == 'POST' && $request->has([$this->username(), 'password'])) {
          try {
              $this->ldap->auth()->bind($request->input($this->username()), $request->input('password'));
              // Successfully bound to server.

          } catch (\Adldap\Auth\BindException $e) {
              // There was an issue binding to the LDAP server.
              Log::critical("BindException");
              
          }
        }
    }
    
    public function username()
    {
        return 'username';
    }
}
