<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Banner;
use Validator;

class BannerController extends Controller {

    public function store(Request $request) {
        Validator::make($request->all(), [
            'url' => 'required',
            'category' => 'required',
            'label' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'image1' => 'required|image|max:2000',
            //'image2' => 'required|image|max:2000'
        ])->validate();
        $banner = new Banner;
        $banner->url = $request->url;
        $banner->category = $request->category;
        $banner->label = $request->label;
        $banner->location = $request->location;
        $banner->start_date = $request->start_date;
        $banner->end_date = $request->end_date;
        $banner->save();

        if ($request->hasFile('image1')) {
            $file = $request->image1;
            $banner->saveImage1($file);
        }
        
        if ($request->hasFile('image2')) {
            $file = $request->image2;
            $banner->saveImage1($file);
        }
        //log_action('banner', $banner->id, 'create');
        return redirect('admin/banner/index');
    }

    public function update(Request $request, $id) {
        Validator::make($request->all(), [
            'url' => 'required',
            'category' => 'required',
            'label' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ])->validate();
        $banner = Banner::find($id);
        $banner->url = $request->url;
        $banner->category = $request->category;
        $banner->label = $request->label;
        $banner->location = $request->location;
        $banner->start_date = $request->start_date;
        $banner->end_date = $request->end_date;
        $banner->save();
        return redirect('admin/banner/edit/' . $id)->with('msg', 'The banner has been updated. ');
    }

    public function index() {
        return view('banner/index');
    }

    public function create() {
        return view('banner/create');
    }

    public function edit($id) {
        $banner = Banner::find($id);
        return view('banner/edit', array('banner' => $banner));
    }

    public function view($id) {
        $banner = Banner::find($id);
        return view('banner/view', array('banner' => $banner));
    }

    public function active($id) {
        $banner = Banner::find($id);
        $banner->isactive = true;
        $banner->save();
        log_action('banner', $banner->id, 'active');
        return redirect('admin/banner/index')->with('msg', 'Banner has been activated. ');
    }

    public function delete($id) {
        $banner = Banner::find($id);
        $banner->isactive = false;
        $banner->save();
        log_action('banner', $banner->id, 'softdelete');
        return redirect('admin/banner/index')->with('msg', 'Banner has been soft deleted. ');
    }

}
