<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Course;
use App\Model\Trainer;
use App\Model\Provider;
use Validator;
use Auth;

class CourseController extends Controller {

    public function store(Request $request) {
        Validator::make($request->all(), ['title' => 'required',
            'start_date' => 'required', 'end_date' => 'required',
            'desc' => 'required', 'provider' => 'required'], ['desc.required' => 'Course Description is empty. '])->validate();
        $course = new Course;
        $course->title = $request->title;
        $course->closing_date = $request->clo_date;
        $course->duration = $request->duration;
        $course->category = $request->category;
        $course->type = $request->type;
        $course->sub_type = $request->sub_type;
        $course->fee_before = $request->fee_before;
        $course->fee_after = $request->fee_after;
        $course->fee = $request->fee;
        $course->venue = $request->venue;
        $course->date = $request->date;
        $course->time = $request->time;
        $course->sign_up_url = $request->sign_up_url;
        $course->desc = $request->desc;
        $course->provider_id = $request->provider;
        $course->start_date = $request->start_date;
        $course->end_date = $request->end_date;
        $course->save();

        if ($request->hasFile('banner1')) {
            $file = $request->banner1;
            $course->saveImage1($file);
        }
        $this->attach_trainer($course->id, $request->trainer);
        //log_action('course', $course->id, 'create');
        return redirect('admin/course/index')->with('msg', 'The course has been added. ');
    }

    public function update(Request $request, $id) {
        Validator::make($request->all(), ['title' => 'required',
            'start_date' => 'required', 'end_date' => 'required',
            'desc' => 'required', 'provider' => 'required'], ['desc.required' => 'Course Description is empty. '])->validate();
        $course = Course::find($id);
        $course->title = $request->title;
        $course->closing_date = $request->clo_date;
        $course->duration = $request->duration;
        $course->category = $request->category;
        $course->type = $request->type;
        $course->sub_type = $request->sub_type;
        $course->fee_before = $request->fee_before;
        $course->fee_after = $request->fee_after;
        $course->fee = $request->fee;
        $course->venue = $request->venue;
        $course->date = $request->date;
        $course->time = $request->time;
        $course->sign_up_url = $request->sign_up_url;
        $course->desc = $request->desc;
        $course->provider_id = $request->provider;
        $course->start_date = $request->start_date;
        $course->end_date = $request->end_date;
        $course->save();

        $this->attach_trainer($id, $request->trainer);
        return redirect('admin/course/edit/' . $id)->with('msg', 'The course has been updated. ');
    }

    public function index($keyword = null) {
        if (empty($keyword)) {
            $courses = Course::paginate(10);
            return view('course/index', array('courses' => $courses));
        } else {
            $courses = Course::where('title', 'like', '%' . $keyword . '%')->paginate(10);
            return view('course/index', array('courses' => $courses, 'keyword' => $keyword));
        }
    }

    public function create() {
        $trainers = Trainer::valid()->get();
        $providers = Provider::valid()->get();
        return view('course/create', array('trainers' => $trainers, 'providers' => $providers));
    }

    public function edit($id) {
        $course = Course::find($id);
        $trainers = Trainer::valid()->get();
        $providers = Provider::valid()->get();

        $attached_trainers = array();
        $course_trainers = $course->trainers->toArray();
        foreach ($course_trainers as $i) {
            array_push($attached_trainers, $i["id"]);
        }
        return view('course/edit', array('course' => $course, 'trainers' => $trainers, 'providers' => $providers, 'attached_trainers' => $attached_trainers));
    }

    public function view($id) {
        if (Auth::check()) {
            $course = Course::find($id);
        } else {
            $course = Course::valid()->where('id', $id)->firstOrFail();
        }
        $trainers = $course->trainers;
        $providers = Provider::valid()->orderBy('name', 'asc')->get();
        return view('course/view', array('course' => $course, 'trainers' => $trainers, 
            'providers' => $providers));

    }

    public function active($id) {
        $course = Course::find($id);
        $course->isactive = true;
        $course->save();
        log_action('course', $course->id, 'active');
        return redirect('admin/course/index')->with('msg', 'Course has been activated. ');
    }

    public function delete($id) {
        $course = Course::find($id);
        $course->isactive = false;
        $course->save();
        log_action('course', $course->id, 'softdelete');
        return redirect('admin/course/index')->with('msg', 'Course has been soft deleted. ');
    }

    public function attach_trainer($id, $array) {
        $course = Course::find($id);
        $course->trainers()->sync($array);
    }

}
