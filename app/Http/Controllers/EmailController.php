<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Model\Enquiry;
use App\Mail\EnquiryMail;
use Illuminate\Http\Request;
use Validator;

class EmailController extends Controller {

    public function enquiry(Request $request) {
        $response = $_POST["g-recaptcha-response"];
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $para = array(
            'secret' => '6LcnxV0UAAAAAMwZxkpJJwW8ZWAfbNL8QHV_r6X1',
            'response' => $response
        );
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-Type: application/x-www-form-urlencoded\r\n',
                'content' => http_build_query($para)
            )
        );
        $context = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success = json_decode($verify);
        if ($captcha_success->success == false) {
            return back()->withErrors('Your captcha is not valid. ');
        }
        Validator::make($request->all(), ['name' => 'required', 'nature' => 'required', 'email' => 'required', "message" => 'required', 'provider' => 'required'])->validate();
        $enquiry = new Enquiry();
        $enquiry->name = $request->name;
        $enquiry->email = $request->email;
        //$contact = $enquiry->contact = $request->contact;
        $enquiry->message = $request->message;
        $nature = "Nature of Enquiry: \r\n";
        foreach ($request->nature as $n) {
            $nature = $nature . " " . $n . " /";
        }
        $enquiry->nature = $nature;
        $enquiry->provider_id = $request->provider;
        $enquiry->course_id = $request->course;
        $enquiry->save();

        if (env('APP_ENV') == 'production') {
            $to = $enquiry->provider->email;
        } else {
            $to = "xcheng@sph.com.sg";
        }
        //try {
            Mail::to($to)->send(new EnquiryMail($enquiry));
        //} catch (\Exception $e) {
            //log_action("enquiry", $enquiry->id, "error");
        //}
        return back()->with('msg', 'Your enquiry has been submitted. ');
    }

}
