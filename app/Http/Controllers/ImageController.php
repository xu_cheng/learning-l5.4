<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Model\Banner;
use App\Model\Course;
use App\Model\Trainer;
use App\Model\Provider;
use Validator;

class ImageController extends Controller {

    public function display($item, $file) {
        switch ($item) {
            case "trainer":
                $path = storage_path('app/image/trainer/') . $file;
                break;
            case "provider":
                $path = storage_path('app/image/provider/') . $file;
                break;
            case "course":
                $path = storage_path('app/image/course/') . $file;
                break;
            case "banner":
                $path = storage_path('app/image/banner/') . $file;
                break;
        }
        return response()->file($path);
    }

    public function edit(Request $request) {
        Validator::make($request->all(), ['file' => 'required|image|max:2000',], ['file.required' => 'File is not chosen. '])->validate();
        $id = $request->id;
        $item = $request->item;
        switch ($item) {
            case "trainer":
                $trainer = Trainer::find($id);
                $file = $request->file;
                $trainer->saveImage($file);
                return redirect('admin/trainer/edit/' . $id);
            case "provider":
                $provider = Provider::find($id);
                $file = $request->file;
                $provider->saveImage($file);
                return redirect('admin/provider/edit/' . $id);
            case "course":
                $course = Course::find($id);
                $file = $request->file;
                $course->saveImage1($file);
                return redirect('admin/course/edit/' . $id);
            case "banner":
                $banner = Banner::find($id);
                $image_number = $request->image_number;
                if ($image_number == 1) {
                    $file = $request->file;
                    $banner->saveImage1($file);
                }
                if ($image_number == 2) {
                    $file = $request->file;
                    $banner->saveImage2($file);
                }
                return redirect('admin/banner/edit/' . $id);
        }
    }

    public function upload(Request $request) {
        //$file = $request->file('file');
        //Storage::disk('s3')->put('cxu104/test', $file);

        $path = Storage::putFileAs(
            'cxu104', $request->file('file'), "image_1"
        );
    }

}
