<?php

namespace App\Http\Controllers;

use App\Model\Course;
use App\Model\Provider;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Description of ImportController
 *
 * @author xcheng
 */
class ImportController {

    public function course() {
        $result = Excel::load(storage_path('app/excel/') . 'STJobs Learning Workshop Detail Form.csv', function($reader) {
                    
                })->get();

        $category_list = config('constant.category');
        $type_list = config('constant.type');

        $i = 2;
        //basic validate 
        foreach ($result as $row) {
            $provider = Provider::valid()->where('name', $row->course_provider)->first();
            if (empty($provider)) {
                return "Provider not found on row " . $i;
            }
            $category = $row->course_category;
            $category_id = array_search($category, $category_list);
            if (empty($category_id)) {
                return "Category not found on row " . $i;
            }
            $type = $row->course_type;
            if ($type != 'Higher Education' && $type != 'Skillsfuture') {
                return "Type not found on row " . $i;
            }
            $i++;
        }
        foreach ($result as $row) {
            $category = $row->course_category;
            $category_id = array_search($category, $category_list);
            $type = $row->course_type;
            if ($type == 'Higher Education') {
                $type_id = 2;
            }
            if ($type == 'Skillsfuture') {
                $type_id = 3;
            }
            $provider = Provider::valid()->where('name', $row->course_provider)->first();
            $provider_id = $provider->id;
            $course = new Course;
            $course->title = $row->course_title;
            $course->closing_date = date("Y-m-d", strtotime($row->registration_closing_date));
            $course->duration = $row->course_duration;
            $course->category = $category_id;
            $course->type = $type_id;
            if ($type_id == 3) {
                $course->sub_type = 5;
            }
            $course->fee_before = $row->fee_description_before_discountgst;
            $course->fee_after = $row->fee_description_after_discountgst;
            $course->venue = $row->course_venue;
            $course->date = date("Y-m-d", strtotime($row->course_date));
            $course->time = $row->course_time;
            $desc = "<h4>Course Introduction</h4>" . $row->course_introduction . "<br>";
            if (!empty($row->course_benefits)) {
                $desc = $desc . "<h4>Course Benefits</h4>" . $row->course_benefits . "<br>";
            }
            if (!empty($row->course_pre_requisites)) {
                $desc = $desc . "<h4>Course Pre-Requisites</h4>" . $row->course_pre_requisites . "<br>";
            }
            if (!empty($row->target_audiencewho_should_attend)) {
                $desc = $desc . "<h4>Target Audience/Who Should Attend</h4>" . $row->target_audiencewho_should_attend . "<br>";
            }
            if (!empty($row->course_outline)) {
                $desc = $desc . "<h4>Course Outline</h4>" . $row->course_outline . "<br>";
            }
            if (!empty($row->available_course_sessions)) {
                $desc = $desc . "<h4>Available Course Sessions</h4>" . $row->available_course_sessions . "<br>";
            }
            $course->desc = $desc;
            $course->provider_id = $provider_id;
            $course->start_date = date('Y-m-d');
            $course->end_date = date('Y-m-d', strtotime('+60 days'));
            $course->save();
        }

        return "Import complete. ";
    }

}
