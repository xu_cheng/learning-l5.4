<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Model\Course;
use App\Model\Provider;

class PageController extends Controller {

    public function workshop() {
        $courses = Course::valid()->where('type', 1)->paginate(6);
        $providers = Provider::valid()->orderBy('name', 'asc')->get();
        return view('page/workshop', array('courses' => $courses, 'providers' => $providers));
    }

    public function homepage() {
        $upcoming_workshop = Course::valid()->where('type', 1)->inRandomOrder()->get()->take(3);
        $recommended_course = Course::valid()->where('type', '<>', 1)->inRandomOrder()->get()->take(3);
        $providers = Provider::valid()->orderBy('name', 'asc')->get();
        return view('page/homepage', array('upcoming_workshop' => $upcoming_workshop, 
            'recommended_course' => $recommended_course, 'providers' => $providers));
    }

    public function education() {
        $diploma_course = Course::valid()->where('sub_type', 1)->inRandomOrder()->get()->take(3);
        $degree_course = Course::valid()->where('sub_type', 2)->inRandomOrder()->get()->take(3);
        $certifications_course = Course::valid()->where('sub_type', 3)->inRandomOrder()->get()->take(3);
        $master_course = Course::valid()->where('sub_type', 4)->inRandomOrder()->get()->take(3);
        $providers = Provider::valid()->orderBy('name', 'asc')->get();
        return view('page/education', array('diploma_course' => $diploma_course, 'degree_course' => $degree_course, 'certifications_course' => $certifications_course, 'master_course' => $master_course, 'providers' => $providers));
    }

    public function training() {
        $online_course = Course::valid()->where('sub_type', 5)->inRandomOrder()->get()->take(3);
        $skillfuture_course = Course::where('sub_type', 5)->inRandomOrder()->get()->take(3);
        $providers = Provider::valid()->orderBy('name', 'asc')->get();
        return view('page/training', array('online_course' => $online_course, 'skillfuture_course' => $skillfuture_course, 'providers' => $providers));
    }
    
    public function resource() {
        $page =1;
        if (isset($_GET["page"])){
            $page = $_GET["page"];
        }
        $ch = curl_init();
        $timeout = 10;
        $url = "http://www.stjobs.sg/career-resources/feeds/latest-articles-by-category.php?cat=66&limit=6&page=". $page;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch); 
        $articles  = json_decode($data);
        $pagin = new LengthAwarePaginator($articles, 10, 6, $page, ['path'=> route('resource')]);
        $providers = Provider::valid()->orderBy('name', 'asc')->get();
        return view('page/resource', array('articles' => $articles->data , 'pagin' =>$pagin, 
            'providers' => $providers));
    }

    public function search() {
        $query = Course::valid();

        if (!isset($_GET['search_key']) && !isset($_GET['course_category']) 
            && !isset($_GET['course_type']) && !isset($_GET['provider_id'])) {
            return view('errors.404');
        }
        
        $keyword = isset($_GET['search_key']) ? trim($_GET['search_key']) : '';
        if (!empty($keyword)) {
            $query = $query->where(function($query) use ($keyword){
                $query->where('desc', 'like', '%' . $keyword . '%')
                ->orWhere('title', 'like', '%' . $keyword . '%');
            });
        }
        $category = isset($_GET['course_category']) ? $_GET['course_category'] : '';
        if (!empty($category)) {
            $query = $query->where('category', '=', $category);
        }
        $type = isset($_GET['course_type']) ? $_GET['course_type'] : '';
        if (!empty($type)) {
            if ($type == "workshop") {
                $query = $query->where('type', '=', 1);
            } else if ($type == "courses") {
                $query = $query->where('type', '<>', 1);
            } else {
                $query = $query->where('sub_type', '=', $type);
            }
        }
        $provider = isset($_GET['provider_id']) ? $_GET['provider_id'] : '';
        if (!empty($provider)) {
            $query = $query->where('provider_id', '=', $provider);
        }

        $courses = $query->paginate(6)->appends(['search_key' => $keyword, 'course_category' => $category, 'course_type' => $type]);
        $providers = Provider::valid()->orderBy('name', 'asc')->get();
        return view('page/search_result', array('courses' => $courses, 'providers' => $providers));
    }

}
