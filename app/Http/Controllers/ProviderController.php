<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Provider;
use Validator;

class ProviderController extends Controller {

    public function store(Request $request) {
        Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'desc' => 'required',
            'logo' => 'required|image|max:2000',
                ], ['desc.required' => 'Description is empty'])->validate();
        $provider = new Provider;
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->url = $request->url;
        $provider->address1 = $request->address1;
        $provider->address2 = $request->address2;
        $provider->address3 = $request->address3;
        $provider->contact = $request->contact;
        $provider->postcode = $request->postcode;
        $provider->desc = $request->desc;
        $provider->isfeature = $request->isfeature;
        $provider->save();

        //save logo image
        if ($request->hasFile('logo')) {
            $extension = $request->logo->extension();
            $filename = $provider->id . "_provider." . $extension;
            try {
                intervention_resize(null, 50, $request->logo->path(), storage_path('app/image/provider/' . $filename));
            } catch (\Exception $e) {
                log_action('provider', $provider->id, 'image compress error');
            }
            $provider->logo = $filename;
            $provider->save();
        }
        //log_action('provider', $provider->id, 'create');
        return redirect('admin/provider/index')->with('msg', 'The provider has been added. ');
    }

    public function update(Request $request, $id) {
        Validator::make($request->all(), ['name' => 'required', 'email' => 'required', 'desc' => 'required'], ['desc.required' => 'Description is empty'])->validate();
        $provider = Provider::find($id);
        $provider->name = $request->name;
        $provider->email = $request->email;
        $provider->url = $request->url;
        $provider->address1 = $request->address1;
        $provider->address2 = $request->address2;
        $provider->address3 = $request->address3;
        $provider->contact = $request->contact;
        $provider->postcode = $request->postcode;
        $provider->desc = $request->desc;
        $provider->isfeature = $request->isfeature;
        $provider->save();
        return redirect('admin/provider/edit/' . $id)->with('msg', 'The provider has been updated. ');
    }

    public function index($keyword = null) {
        if (empty($keyword)) {
            $providers = Provider::paginate(10);
            return view('provider/index', array('providers' => $providers));
        } else {
            $providers = Provider::where('name', 'like', '%' . $keyword . '%')->paginate(10);
            return view('provider/index', array('providers' => $providers, 'keyword' => $keyword));
        }
    }

    public function listing($id, $keyword = null) {
        if (empty($keyword)) {
            $provider = Provider::find($id);
            $courses = $provider->admin_courses()->latest()->paginate(15);
            return view('provider.list', array('courses' => $courses, 'provider'=>$provider));
        } else {
            $provider = Provider::find($id);
            $courses = $provider->admin_courses()->where('title', 'like', '%' . $keyword . '%')->latest()->paginate(15);
            return view('provider.list', array('courses' => $courses, 'provider'=>$provider));
        }
    }

    public function create() {
        return view('provider/create');
    }

    public function edit($id) {
        $provider = Provider::find($id);
        return view('provider/edit', array('provider' => $provider));
    }

    public function view($id) {
        $provider = Provider::find($id);
        return view('provider/view', array('provider' => $provider));
    } 

    public function active($id) {
        $provider = Provider::find($id);
        $provider->isactive = true;
        $provider->save();
        if (isset($_GET["courses"])){
            foreach($provider->admin_courses()->get() as $course){
                $course->active();
            }
            log_action('provider', $provider->id, 'active courses');
        }
        log_action('provider', $provider->id, 'active');
        return redirect('admin/provider/index')->with('msg', 'Provider has been activated. ');
    }

    public function delete($id) {
        $provider = Provider::find($id);
        $provider->isactive = false;
        $provider->save();
        if (isset($_GET["courses"])){
            foreach($provider->admin_courses()->get() as $course){
                $course->delete();
                log_action('provider', $provider->id, 'softdelete courses');
            }
        }
        log_action('provider', $provider->id, 'softdelete');
        return redirect('admin/provider/index')->with('msg', 'Provider has been soft deleted. ');
    }

    public function confirm($id) {
        $provider = Provider::find($id);
        return view('provider/confirm', array('provider' => $provider));
    }
}
