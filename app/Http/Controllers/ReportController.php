<?php

namespace App\Http\Controllers;

use App\Model\Enquiry;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Description of ImportController
 *
 * @author xcheng
 */
class ReportController {

    public function enquiry() {
        Excel::create('enquiry', function($excel) {
            $excel->sheet('enquiry', function($sheet) {
                $sheet->appendRow(['Name', 'Email', 'Course Title', 'Provider', 'Date']);
                $enquiries = Enquiry::all();
                foreach ($enquiries as $enquiry) {
                    $sheet->appendRow(array(
                        $enquiry->name, $enquiry->email, $enquiry->course->title, $enquiry->provider->name, $enquiry->created_at->toDateString()
                    ));
                }
            });
        })->download('xlsx');
    }

}
