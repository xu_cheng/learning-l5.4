<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Trainer;
use Validator;

class TrainerController extends Controller {

    public function store(Request $request) {
        Validator::make($request->all(), [
            'name' => 'required',
            'desc' => 'required',
            'photo' => 'nullable|image|max:2000',
                ], ['desc.required' => 'Description is empty'])->validate();
        $trainer = new Trainer;
        $trainer->name = $request->name;
        $trainer->desc = $request->desc;
        $trainer->save();

        if ($request->hasFile('photo')) {
            $extension = $request->photo->extension();
            $filename = $trainer->id . "_trainer." . $extension;
            try {
                intervention_resize(150, 150, $request->photo->path(), storage_path('app/image/trainer/' . $filename));
            } catch (\Exception $e) {
                log_action('trainer', $trainer->id, 'image compress error');
            }
            $trainer->photo = $filename;
            $trainer->save();
        }
        //log_action('trainer', $trainer->id, 'create');
        return redirect('admin/trainer/index')->with('msg', 'Trainer has been added. ');
    }

    public function update(Request $request, $id) {
        Validator::make($request->all(), ['name' => 'required', 'desc' => 'required'], ['desc.required' => 'Description is empty'])->validate();
        $trainer = Trainer::find($id);
        $trainer->name = $request->name;
        $trainer->desc = $request->desc;
        $trainer->save();
        return redirect('admin/trainer/edit/' . $id)->with('msg', 'Trainer has been updated. ');
    }

    public function index($keyword = null) {
        if (empty($keyword)) {
            $trainers = Trainer::paginate(6);
            return view('trainer/index', array('trainers' => $trainers));
        } else {
            $trainers = Trainer::where('name', 'like', '%' . $keyword . '%')->paginate(6);
            return view('trainer/index', array('trainers' => $trainers, 'keyword' => $keyword));
        }
    }

    public function create() {
        return view('trainer/create');
    }

    public function edit($id) {
        $trainer = Trainer::find($id);
        return view('trainer/edit', array('trainer' => $trainer));
    }

    public function view($id) {
        $trainer = Trainer::find($id);
        return view('trainer/view', array('trainer' => $trainer));
    }

    public function active($id) {
        $trainer = Trainer::find($id);
        $trainer->isactive = true;
        $trainer->save();
        log_action('trainer', $trainer->id, 'active');
        return redirect('admin/trainer/index')->with('msg', 'Trainer ' . $trainer->name . ' has been activated. ');
    }

    public function delete($id) {
        $trainer = Trainer::find($id);
        $trainer->isactive = false;
        $trainer->save();
        log_action('trainer', $trainer->id, 'softdelete');
        return redirect('admin/trainer/index')->with('msg', 'Trainer ' . $trainer->name . ' has been soft deleted. ');
    }

}
