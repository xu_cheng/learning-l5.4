<?php

namespace App\Mail;

use App\Model\Enquiry;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnquiryMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    protected $enquiry, $course_title;
    
    public function __construct(Enquiry $enquiry)
    {
        $this->enquiry = $enquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $data = array('nature' => $this->enquiry->nature, 'course'=> $this->enquiry->course->title, 'name' => $this->enquiry->name, 'email' =>$this->enquiry->email, 'msg' => $this->enquiry->message);

        return $this
                ->from(['customercare@stjobs.sg'])
                ->view('email.enquiry')
                ->with($data);
    }
}
