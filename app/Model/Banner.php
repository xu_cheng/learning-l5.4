<?php

namespace App\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public function scopeValid($query)
    {
    	$now = date('Y-m-d');
        return $query->where([['isactive', 1], ['start_date', '<=' , $now], ['end_date', '>=' , $now]]);
    }
    
    public function saveImage1(UploadedFile $file){
        $extension = $file->extension();
        $filename = $this->id . "_banner1." . $extension;
        if ($extension == "gif") {
            $file->storeAs('image/banner/', $filename);
        } else {
            try {
                intervention_resize(720, 90, $file->path(), storage_path('app/image/banner/' . $filename));
            } catch (\Exception $e) {
                log_action('banner', $this->id, 'image1 compress error');
            }
        }
        $this->image1 = $filename;
        $this->save();
    }
    
    public function saveImage2(UploadedFile $file){
        $extension = $file->extension();
        $filename = $this->id . "_banner2." . $extension;
        if ($extension == "gif") {
            $file->storeAs('image/banner/', $filename);
        } else {
            try {
                intervention_resize(320, 50, $file->path(), storage_path('app/image/banner/' . $filename));
            } catch (\Exception $e) {
                log_action('banner', $this->id, 'image2 compress error');
            }
        }
        $this->image2 = $filename;
        $this->save();
    }
}
