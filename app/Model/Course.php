<?php

namespace App\Model;

use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Course extends Model {

    protected $table = 'course';

    public function trainers() {
        return $this->belongsToMany('App\Model\Trainer')->valid();
    }

    public function provider() {
        if (Auth::check()) {
            return $this->belongsTo('App\Model\Provider');
        } 
        return $this->belongsTo('App\Model\Provider')->valid();

    }

    public function scopeValid($query) {
        $now = date('Y-m-d');
        return $query->where([['isactive', 1], ['start_date', '<=', $now], ['end_date', '>=', $now]]);
    }

    public function saveImage1(UploadedFile $file) {
        $extension = $file->extension();
        $filename = $this->id . "_banner_1." . $extension;
        if ($extension == "gif") {
            $file->storeAs('image/course/', $filename);
        } else {
            try {
                intervention_resize(360, 240, $file->path(), storage_path('app/image/course/' . $filename));
            } catch (\Exception $e) {
                log_action('course', $this->id, 'banner1 compress error');
            }
        }
        $this->banner_one = $filename;
        $this->save();
    }
    
    public function active(){
        $this->isactive = true;
        $this->save();
    }
    
    public function delete(){
        $this->isactive = false;
        $this->save();
    }

}
