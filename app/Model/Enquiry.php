<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $table = 'enquiry';
    
    public function provider() {
        return $this->belongsTo('App\Model\Provider');
    }
    
    public function course() {
        return $this->belongsTo('App\Model\Course');
    }
    
}
