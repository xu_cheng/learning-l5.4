<?php

namespace App\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model {

    protected $table = 'provider';

    public function courses() {
        return $this->hasMany('App\Model\Course')->valid();
    }
    
    public function admin_courses() {
        return $this->hasMany('App\Model\Course');
    }

    public function scopeValid($query) {
        return $query->where('isactive', 1);
    }

    public function saveImage(UploadedFile $file) {
        $extension = $file->extension();
        $filename = $this->id . "_provider." . $extension;
        if ($extension == "gif") {
            $file->storeAs('image/provider/', $filename);
        } else {
            try {
                intervention_resize(null, 50, $file->path(), storage_path('app/image/provider/' . $filename));
            } catch (\Exception $e) {
                log_action('provider', $this->id, 'image compress error');
            }
        }
        $this->logo = $filename;
        $this->save();
    }

}
