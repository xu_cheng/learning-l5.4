<?php

namespace App\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;

class Trainer extends Model {

    protected $table = 'trainer';

    public function courses() {
        return $this->belongsToMany('App\Model\Course')->valid();
    }

    public function scopeValid($query) {
        return $query->where('isactive', 1);
    }

    public function saveImage(UploadedFile $file) {
        $extension = $file->extension();
        $filename = $this->id . "_trainer." . $extension;
        if ($extension == "gif") {
            $file->storeAs('image/trainer/', $filename);
        } else {
            try {
                intervention_resize(150, 150, $file->path(), storage_path('app/image/trainer/' . $filename));
            } catch (\Exception $e) {
                log_action('trainer', $this->id, 'image compress error');
            }
        }
        $this->photo = $filename;
        $this->save();
    }

}
