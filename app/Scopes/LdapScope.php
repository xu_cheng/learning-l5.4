<?php

namespace App\Scopes;

use Adldap\Query\Builder;
use Adldap\Laravel\Scopes\ScopeInterface;

class LdapScope implements ScopeInterface
{
    /**
     * Apply the scope to a given LDAP query builder.
     *
     * @param Builder $query
     *
     * @return void
     */
    public function apply(Builder $query)
    {
        // The distinguished name of our LDAP group.
        $learning = 'CN=GG-APP-NM-STJ-LEARNING-DEV-ADMIN,OU=SPH,DC=sphinet,DC=com,DC=sg';
        
        $query->whereMemberOf($learning);
    }
}