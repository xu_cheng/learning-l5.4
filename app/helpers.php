<?php

use Intervention\Image\Facades\Imgae;
use Carbon\Carbon;
use App\Model\Log;

if (!function_exists('intervention_resize')) {

    /**
     * Image resize using Intervention.
     *
     * @param  numeric  $width
     * @param  numeric  $height
     * @param  string  $inFilePath
     * @param  string  $outFilePath
     */
    function intervention_resize($width, $height, $inFilePath, $outFilePath) {
        $img = Image::make($inFilePath);
        $img->interlace();
        $img->resize($width, $height, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($outFilePath, 89);
    }

}

if (!function_exists('shorten_text')) {

    /**
     * Shorten text by stripping html tags.
     *
     * @param  string  $text
     * @param  numeric  $length
     * @param  boolean  $strip
     * @return  string
     */
    function shorten_text($text, $length = 150, $strip = true) {
        if ($strip)
            $text = strip_tags(trim($text));
        if (strlen($text) > $length) {
            $text = $text . " ";
            $text = substr($text, 0, $length);
            $text = substr($text, 0, strrpos($text, ' '));
            $text = $text . "...";
        }
        return $text;
    }

}

if (!function_exists('carbon_nullableFormat')) {

    /**
     * Create a Carbon instance from a specific format.
     *
     * @param string                    $format
     * @param string                    $time
     * @param \DateTimeZone|string|null $tz
     * @param Carbon $default
     *
     * @throws \InvalidArgumentException
     *
     * @return Carbon|string|null
     */
    function carbon_nullableFormat($format, $time, $tz = null, $default = null) {
        $result = $time;
        if (!empty($time))
            $result = Carbon::createFromFormat($format, $time);
        else if (isset($default))
            $result = $default;
        return $result;
    }

}

if (!function_exists('date_to_string')) {

    /**
     * Create DateTime String from Carbon object.
     *
     * @param string                    $format
     * @param Carbon                    $carbon
     * @param string                    $default
     *
     * @return string
     */
    function carbon_to_string($format, $carbon, $default = '') {
        $result = $default;
        if ($carbon) {
            if (empty($format))
                $result = $carbon->toDateTimeString();
            else
                $result = $carbon->format($format);
        }
        return $result;
    }

}

if (!function_exists('log_action')) {

    /**
     * Save the action of admin into log table
     *
     * @param string  $item
     *
     * @param integer $item_id
     *
     * @param string  $action
     *
     *
     */
    function log_action($item, $item_id, $action) {
        try {
            $log = new Log;
            $log->item = $item;
            $log->item_id = $item_id;
            $log->action = $action;
            $log->account = Auth::user()->id;
            $log->save();
        } catch (\Exception $e) {
            $log = new Log;
            $log->item = $item;
            $log->item_id = $item_id;
            $log->action = $action;
            $log->account = '-1';
            $log->save();
        }
    }

}