<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->text('desc');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('banner_one', 100)->nullable();
            $table->string('banner_two', 100)->nullable();
            $table->tinyInteger('type');
            $table->tinyInteger('sub_type')->nullable();
            $table->tinyInteger('category');
            $table->date('closing_date')->nullable();
            $table->string('duration', 200)->nullable();
            $table->string('fee_before', 50)->nullable();
            $table->string('fee_after', 50)->nullable();
            $table->integer('fee')->nullable();
            $table->string('venue', 200)->nullable();
            $table->date('date')->nullable();
            $table->string('sign_up_url', 255)->nullable();
            $table->string('time', 50)->nullable();
            $table->boolean('isactive')->default(1);
            $table->integer('provider_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course');
    }
}
