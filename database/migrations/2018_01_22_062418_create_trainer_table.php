<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainer', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->text('desc');
            $table->string('photo', 100)->nullable();
            $table->boolean('isactive')->default(1);
            $table->timestamps();
        });
        Schema::create('course_trainer', function (Blueprint $table) {
            $table->integer('trainer_id');
            $table->integer('course_id');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_trainer');
        Schema::dropIfExists('trainer');
    }
}
