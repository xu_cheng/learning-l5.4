<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProviderTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('provider', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->text('desc');
            $table->string('contact', 100)->nullable();
            $table->string('email', 100);
            $table->string('url', 255)->nullable();
            $table->string('address1', 100)->nullable();
            $table->string('address2', 100)->nullable();
            $table->string('address3', 100)->nullable();
            $table->integer('postcode')->nullable();
            $table->string('logo', 100)->nullable();
            $table->boolean('isfeature')->default(0);
            $table->boolean('isactive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('provider');
    }

}
