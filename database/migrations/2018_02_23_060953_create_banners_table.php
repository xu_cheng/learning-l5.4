<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('url', 255);
            $table->integer('location')->nullable();
            $table->string('category')->nullable();
            $table->string('label')->nullable();
            $table->date('start_date')->useCurrent();
            $table->date('end_date')->useCurrent();
            $table->boolean('isactive')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('banners');
    }

}
