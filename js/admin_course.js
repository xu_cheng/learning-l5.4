function sub_type_select() {
    type = $('#type').find(":selected").val();
    if (type == 2) {
        document.getElementById("sub_type_box").innerHTML = "<label>Sub Type</label>" +
                "<select name='sub_type' class='form-control'>" +
                "<option value='1'>Diploma</option>" +
                "<option value='2'>Degree</option>" +
                "<option value='3'>Certifications</option>" +
                "<option value='4'>Master/PHD</option></select>";
        $('#signupurl').hide();
    } else if (type == 3) {
        document.getElementById("sub_type_box").innerHTML = "<label>Sub Type</label>" +
                "<select name='sub_type' class='form-control'>" +
                //"<option value='5'>Online Course</option>" +
                "<option value='6'>Skills Future</option></select>";
        $('#signupurl').hide();
    } else {
        document.getElementById("sub_type_box").innerHTML = "";
        $('#signupurl').show();
    }
}
$(document).ready(function () {
    $('#trainer').select2();
    $("#type").change(sub_type_select);
    $('#category, #provider').select2({
        theme: "bootstrap"
    });
});

