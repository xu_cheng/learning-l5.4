<li class="course-box small ">
	<a href="<?php echo $course->getDetailPageURL(); ?>" onclick="_gaq.push(['_trackEvent', 'udemy','detail','<?php echo $course->getCategoryTitle();?>']);" class="mask" target="_blank">
        <span>
            <span class="lec-info fxdc">
                <span class="row-one"><?php echo $course->getNumLectures(); ?> lectures</span>
                <span class="row-two"><?php echo $course->getContentInfo(); ?> video</span>
            </span>
            <span class="course-thumb pos-r dib">
                <img src="<?php echo $course->getImage("240x135"); ?>" alt="<?php echo $course->getTitle(); ?>">
                <span class="avatars-list">
					<span class="avatar">
						<?php foreach($course->getInstructors() as $instructor){ ?>
						<img src="<?php echo $instructor->getImage("50x50"); ?>" alt="<?php echo $instructor->getDisplayName(); ?>">
						<?php } ?>
					</span>
                </span>
            </span>
        </span>
        <span class="box-second-row">
            <span class="title ellipsis-2lines"><?php echo $course->getTitle(); ?></span>
            <span class="dib inst-name ellipsis-2lines">
				<?php 
					if(count($course->getInstructors())==1){
						$instructor = $course->getInstructors();
						echo $instructor[0]->getDisplayName().','.$instructor[0]->getJobTitle();
					}
					else if(count($course->getInstructors())>1){
						$arrInstructorName = array();
						foreach($course->getInstructors() as $instructor){ 
							$arrInstructorName[] = $instructor->getDisplayName();
						}
						echo implode(',',$arrInstructorName);
					}
				?>
			</span>
            <span class="reviews-row db p5-10 fxw">
                <span class="review-count">
                    <span class="s-rating smaller static">
                        <span style="width:<?php echo $course->getAvgRating(); ?>%"></span>
                    </span>
                    <span>(<?php echo $course->getNumReviews(); ?>)</span>
                </span>
            </span>
            <span class="text-throught fxac mh36 mh0-xs mt5-xs ml10 ml0-xs">
				<?php $discount = $course->getDiscountInfo(); ?>
				<span class="price <?php if($discount) echo 'line-through'; ?>"><?php echo $course->getPrice(); ?></span> 
				<?php if($discount){?><span class="price-discount"><?php echo $discount['text'].' ('.$discount['percentage'].'% Off)'; ?></span><?php } ?>
            </span>
        </span>
    </a>
</li>