<footer class="site-footer">
    <div class="container">
        <div class="row site-footer-top">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <h4>Course Seekers</h4>
                <ul>
                    <li><a href="http://learning.stjobs.sg/workshop">Workshops</a></li>
                    <li><a href="http://learning.stjobs.sg/education">Higher Education Courses</a></li>
                    <li><a href="http://learning.stjobs.sg/onlinecourses">Online Courses</a></li>
                    <li><a href="http://learning.stjobs.sg/search?search_key=&course_type=5&course_category=">SkillsFuture Courses</a></li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <h4>Providers</h4>
                <ul>
                    <li><a href="http://www.stjobs.sg/info/contactus" target="_blank">Advertise With Us</a></li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <h4>Contact Us</h4>
                <ul>
                    <li class="site-footer-contact"><i class="fa fa-phone"></i>6319 8877</li>
                    <li class="site-footer-contact"><i class="fa fa-envelope"></i><a href="mailto:customercare@stjobs.sg" target="_blank">customercare@stjobs.sg</a></li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <h4>Follow Us</h4>
                <ul>
                    <li class="site-footer-social"><a href="http://www.facebook.com/STJobs.SG" target="_blank"><i class="fa fa-facebook"></i></a> <a href="http://instagram.com/stjobssg" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div><!--/.row-->
        <style>
            .site-footer-bottom {
                border-top: solid 1px rgba(255,255,255,0.1);
                padding-top: 30px;
            }
        </style>
        <div class="row site-footer-bottom">
            <div class="col-xs-12 col-md-6 col-lg-7 site-footer-branding">
                <div class="site-footer-branding-inner">
                    <div class="site-footer-branding-logo"><a href="http://www.stjobs.sg" target="_blank"><img src="images/logo-footer.png" class="img-responsive" alt="STJobs"/></a></div>
                    <div class="site-footer-branding-info">
                        <p>&copy; 2017 Singapore Press Holdings Ltd.</p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 col-lg-5 site-footer-terms">
<!--            <a href="#">About Us</a><span>|</span>-->
                <a href="http://www.stjobs.sg/info/terms" target="_blank">Terms &amp; Conditions</a><span>|</span>
                <a href="http://sph.com.sg/legal/sph_privacy.html" target="_blank">Privacy Policy</a>
            </div>
        </div><!--/.row-->
    </div><!--/.container-->
</footer>