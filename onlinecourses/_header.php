<head>
    <link rel="stylesheet"  media="all" type="text/css" href="assets/css/discover.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="Content-Language" content="en">
    <!-- Employer Dashboard -->
    <meta name="medium" content="mult" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Custom styles for this template -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <!-- Advanced Select -->
    <link href="assets/css/cs-select.css" rel="stylesheet">

    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- end employer dashboard -->
    <title><?php
        if (isset($pageTtl) && $pageTtl) {
            echo $pageTtl;
        } else {
            echo 'STJobs | Udemy Campaign';
        }
        ?></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script type="text/javascript" src="http://adtag.asiaone.com/tag/stjobs/js/stjobs_gpt_header.js"></script>
    <?php
    if (isset($arrAdditionalStyle) && is_array($arrAdditionalStyle)) {
        foreach ($arrAdditionalStyle as $style) {
            echo $style;
        }
    }
    ?>
</head>

<?php
require_once ("_navigation.php");
?>