<!-- Employer Dashboard -->
<!-- jQuery 2.1.4 -->
<script src="assets/js/jQuery-2.1.4.min.js"></script>

<!-- Advanced Select -->
<script src="assets/js/classie.js"></script>
<script src="assets/js/selectFx.js"></script>

<script language="JavaScript" type="text/javascript" src="https://app.stjobs.sg/tagging/stjobs.js"></script>
<script>
	(function() {
		[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
			new SelectFx(el);
		} );
	})();
	
	function updateURLParameter(url, param, paramVal){
		var newAdditionalURL = [];
		var tempArray = url.split("?");
		var baseURL = tempArray[0];
		var additionalURL = tempArray[1];
		if (additionalURL) {
			tempArray = additionalURL.split("&");
			for (i=0; i<tempArray.length; i++){
				if(tempArray[i].split('=')[0] != param){
					newAdditionalURL.push(tempArray[i]);
				}
			}
		}
		if(paramVal)newAdditionalURL.push(param + "=" + paramVal);
		return baseURL + "?" + newAdditionalURL.join("&");
	}
	$(document).ready(function() {
		$( '#dl-menu' ).dlmenu();
		
		$( "#txtSearch" ).on( "keypress", function(e) {
			if(e.which == 13) searchCourse();
		});
		
		$( ".btnSearch" ).on( "click", function() {
			searchCourse();
		});
		
		function searchCourse(){
			var txtSearch = jQuery('#txtSearch').val();
			var selCategory = jQuery('#selCategory').val();
			_gaq.push(['_trackEvent', 'udemy','search',selCategory]);
			var url = "listing.php"+window.location.search;
			url = updateURLParameter(url, 'search', encodeURIComponent(txtSearch));
			url = updateURLParameter(url, 'category', encodeURIComponent(selCategory));
			url = updateURLParameter(url, 'page', 1);
			window.location = url;
		}
	});
</script>
 <!-- Employer Dashboard -->