<?php
$learningurl = "http://learning.stjobs.sg";
?>

<link rel="stylesheet" type="text/css" href="<?php echo $learningurl; ?>/css/custom.css">
<link rel="stylesheet" type="text/css" href="<?php echo $learningurl; ?>/css/custom-responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo $learningurl; ?>/css/style.css">

<header class="learning-nav" style="margin-bottom: 120px">
    <nav class="navbar navbar-fixed-top">
        <div class="container">
            <a class="navbar-brand" href="<?php echo $learningurl; ?>/" title="STJobs Learning"><img src="images/logo.jpg" alt="STlearning"></a>

            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo $learningurl; ?>">Home</a></li>
                    <li><a href="<?php echo $learningurl; ?>/workshop">Workshops</a></li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Higher Education<span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-left">
                            <li><a href="<?php echo $learningurl; ?>/search?search_key=&course_type=1&course_category=">Diploma</a></li>
                            <li><a href="<?php echo $learningurl; ?>/search?search_key=&course_type=2&course_category=">Degree</a></li>
                            <li><a href="<?php echo $learningurl; ?>/search?search_key=&course_type=3&course_category=">Certifications</a></li>
                            <li><a href="<?php echo $learningurl; ?>/search?search_key=&course_type=4&course_category=">Masters/PhD</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?php echo $learningurl; ?>/training" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Training Courses<span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-menu-left">
                            <li><a href="<?php echo $learningurl; ?>/onlinecourses">Online Courses</a></li>
                            <li><a href="<?php echo $learningurl; ?>/search?search_key=&course_type=5&course_category=">Skills Future</a></li>
                        </ul>
                    </li>
                    <li><a href="http://www.scholarschoice.com.sg/" target="_blank">Scholarships</a></li>
                    <li><a href="http://www.stjobs.sg/info/contactus" target="_blank">Advertise</a></li>
                </ul>

            </div><!--/.nav-collapse -->
        </div>
    </nav>
</header>

