<div class="ud-coursecarousel">
    <div class="courses-header">
        <div class="left-items">
            <a class='show-all-toggle' href="javascript:void(0)">
				<h4 class="list-title"><?php if(isset($sortingTtl) && $sortingTtl)echo $sortingTtl;?> in <em>"<?php if(isset($categoryTtl) && $categoryTtl)echo $categoryTtl;?>"</em></h4>
            </a>
        </div>
        <div class="right-items">
            <a class="collapse-btn view-all btn" href="<?php echo getCategorySearchURL('',$categoryTtl).'&ordering='.$criteria->ordering;?>" onclick="_gaq.push(['_trackEvent', 'udemy','search','<?php echo $categoryTtl;?>']);">View All</a>
        </div>
    </div>
	<ul class="discover-courses-list channel-courses-list">
		<?php
		$courses = json_decode($courses);
		foreach($courses->results as $item){
			$course = new UdemyCourse($item);
			include('_UdemyCourseCard.php');
		}
		?>
	</ul>
</div>