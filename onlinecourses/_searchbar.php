<div class="advsearch-jobsfeed">
	<div class="container" style="z-index:999; border-radius: 10px;margin-top:20px; margin-bottom:20px;">
		<div class="col-sm-2">
			<center>
				<img src="images/course-partner.png" class="img-responsive hidden-xs"> <img src="images/course-partner-mobile.png" class="img-responsive visible-xs">
			</center>
		</div>
		<div class="col-sm-10 learning-search padding-top-20" >
			<div class="col-sm-6 col-md-6">
				<input id="txtSearch" class="form-control" type="text" value="<?php echo $criteria->search; ?>" placeholder="Search for Courses" style="border: 0px; border-radius:0px; margin-bottom:3px;">
			</div>
			<div class="col-sm-4 col-md-4">
				<section>
					<div class="wrap">
						<select class="cs-select cs-skin-border" id="selCategory">
							<option value="" ></i> All Categories</option>
							<?php 
								foreach($UdemyCategories as $key => $value){ 
									$strSelected = '';
									if($key == $criteria->category)$strSelected = "selected";
							?>
								<option value="<?php echo $key; ?>" <?php echo $strSelected; ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</div>
				</section>
			</div>
			<div class="col-sm-2 col-md-2">
				<span class="input-group-btn" >
					<button id="btnSearch" class="btn btn-success btn-block btnSearch" type="button">Search</button>
				</span>
			</div>
		</div>  <!-- End col-sm-12-->
    </div>
	<div class="clearfix"></div>
</div>