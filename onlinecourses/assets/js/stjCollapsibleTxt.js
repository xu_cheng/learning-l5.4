(function ($) {
	
	$.fn.stjCollapsibleTxt = function( options ) {
		var defaults = {
			height: '190px',
			defaultHide : true,
			buttonContainerCls: '',
			button: {
				cls : "btnShowMore",
				showtext : "Show Detail",
				hidetext : "Hide Detail"
			}, 
			overlaycls: "stjCollapsibleTxt-overlay"
		}; 
		var opts = $.extend( {}, defaults, options );
		
		var init = function ( elem, options ) {
			elem.resize(function(){debugger});
			elem.addClass( "stjCollapsibleTxt-container" );
			elem.height(options.height);
			var overlay = document.createElement( 'div' );
			overlay.className = options.overlaycls;
			elem.append( overlay );
			
			var btn = document.createElement( 'a' );
			btn.className = options.button.cls;
			if(options.defaultHide){
				btn.innerHTML = options.button.showtext;
			}else{
				btn.innerHTML = options.button.hidetext;
				elem.height('auto');
			}
			btn.onclick = function() {
				var height=options.height,text=options.button.showtext;
				if(jQuery(this).html() == options.button.showtext){
					height = 'auto';
					text= options.button.hidetext;
				}
				jQuery(overlay).toggle();
				elem.height(height);
				jQuery(this).html(text);
			};
			
			var btnContainer = document.createElement( 'div' );
			btnContainer.className = options.buttonContainerCls;
			btnContainer.appendChild(btn);
			
			elem.after( btnContainer );
			return elem;
		};
		
		return this.each(function() {
			init(jQuery( this ), opts);
		});
	};
}(jQuery));