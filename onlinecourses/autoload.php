<?php
require_once('./config/config.php'); 
require_once('./config/globalFunction.php'); 
function stLearningAutoload($class_name)
{
	$classdirectory = array ('model');
	foreach($classdirectory as $prefix)
	{ 
	  $path = './'.$prefix . '/' . $class_name . '.php'; 
	  if(file_exists($path))
	  {
	  	require_once $path;
	    return;	  
	  }
	}
}
spl_autoload_register('stLearningAutoload');