<?php
	$STJLearningURL = 'http://learning.stjobs.sg/';
	$STJLearningSearchURL = $STJLearningURL.'courses-search/';
	$OutboundAlertURL = 'outboundalert.php';
	$OutboundAlertDelay = 5000;
	$UdemyDomainURL = 'https://www.udemy.com';
	$UdemyPmTag = 'STJOBS2018';
	$UdemyUTMTrackingCode = 'utm_source=stjobs&utm_medium=web&utm_campaign='.$UdemyPmTag.'&deal_code='.$UdemyPmTag.'&aff_code=E0ITcl1aTHsBSRlxX1dYfQNeG3VOU0B2A0QRdVpA';
	$DomainURL = '';
	$DefaultPageSize = 15;
	$UdemyCategories = array(
		"Development" => "Development" ,
		"Business" => "Business" ,
		"IT & Software" => "IT & Software" ,
		"Office Productivity" => "Office Productivity" ,
		"Personal Development" => "Personal Development" ,
		"Design" => "Design" ,
		"Marketing" => "Marketing" ,
		"Teacher Training" => "Teacher Training" ,
		"Language" => "Language" ,
	);
	$MainPageUdemySort = "best_seller";
	$UdemySorting = array(
		"best_seller" =>"Best Seller",
		"enrollment" =>"Enrollment",
		"trending" =>"Trending"
	);