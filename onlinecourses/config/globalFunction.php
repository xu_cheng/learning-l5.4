<?php
function getCategorySearchURL($key ='',$value = ''){
	global $DomainURL, $UdemyCategories;
	$url = '';
	if($key){
		$url = $DomainURL.'listing.php?category='.urlencode($key);
	}else if($value){
		$url = $DomainURL.'listing.php?category='.urlencode(array_search ($value,$UdemyCategories));
	}
	return $url;
}

function getRandomCategoryKey($num){
	global $UdemyCategories;
	return array_rand($UdemyCategories, $num);
}