<?php
require_once ("autoload.php");
$arrAdditionalStyle = array(
	'<link rel="stylesheet" type="text/css" href="assets/css/video-js.css">',
	'<link rel="stylesheet" type="text/css" href="assets/css/stjCollapsibleTxt.css">'
	);
$course_detail = false;
$pageTtl = '';
if(isset($_GET ['id'])){
	$api = new UdemyApi();
	$course_detail = $api->getCourseDetail($_GET ['id']);
	$course = new UdemyCourse($course_detail);
	$pageTtl = $course->getTitle();
}
require_once ("_header.php");
if($course_detail && $course->getID()){
	$discount = $course->getDiscountInfo();
?>
<div class="clearfix"></div>
<link rel="stylesheet" media="all" type="text/css" href="assets/css/new-landing-page.css"/>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<style>
	.video-js {
		width: 100% !important;
		height: 100% !important;
	}
</style>
<div class="one-col-landing ud-page " >
	<div class="menu-hidden hidden-xs hidden-sm" id="menu_slide">
        <div class="container">
            <div class="row flex-ae">
                <div class="col-md-7 col-sm-12 fs12-lg" id="scrollspy">
                    <ul class="tab-btns nav">
                        <li><a class="tab-btn" href="#desc" target="_self">Description</a></li>
						<li><a class="tab-btn" href="#instructor" target="_self">Instructor</a></li>
						<li><a class="tab-btn" href="#requirements" target="_self">Requirements</a></li>
						<li><a class="tab-btn" href="#what-you-get" target="_self">What you get</a></li>
                        <li><a class="tab-btn" href="#who-should-attend" target="_self">Who should attend</a></li>
                    </ul>
                </div>
                <div class="col-md-5 hidden-sm">
                    <div class="fxac">
    					<a href="<?php echo $course->getUdemyURL($UdemyDomainURL); ?>" class="btn btn-lg m10-0 course-cta btn-warning" onclick="_gaq.push(['_trackEvent', 'udemy','<?php echo $course->getCategoryTitle();?>','<?php echo $course->getUdemyURL('',false);?>']);">I'm interested</a>
                        <div class="m0-10"></div>
                        <div class="flex-ac sliding_price_text_wrapper">
							<div class="price"><?php if($discount)echo $discount['text'];else echo $course->getPrice(); ?></div> 
							<?php if($discount){ ?>
							<div class="save-money discount-wrap--sliding-dropdown" style='margin-left: 10px;'>
								<div class="old-price"><?php echo $course->getPrice(); ?></div>
								<div class="save-money discount-percent--sliding-dropdown"><?php echo $discount['percentage']; ?>% off</div>
							</div>
							<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<div class="main-top">
		<div class="container">
			<div class="row course-desc">
				<div class="col-md-12">
					<h1 class="course-title" ><?php echo $course->getTitle(); ?></h1>
					<div class="ci-d"><?php echo $course->getHeadline(); ?></div>
					<div class="enrolled">
						<span class="rate fxac flex-col-xs" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
							<span class="s-rating smaller static stars">
								<span style="width: <?php echo $course->getAvgRating(); ?>%;"></span>
							</span>
							<span class="rate-count ml10">
								<?php echo $course->getNumReviews(); ?> ratings, <?php echo $course->getNumSubscribers(); ?> students enrolled
							</span>
						</span>
					</div>
					
					<div class="by mb10 flex-col-xs">
						<span class="mr30">
							Instructed by
							<?php foreach($course->getInstructors() as $instructor){ ?>
								<img src="<?php echo $instructor->getImage("50x50"); ?>" alt="<?php echo $instructor->getDisplayName(); ?>">
							<?php } ?>
						</span>
						<span class="cats">
							<a class="cd-ca" href="<?php echo getCategorySearchURL('',$course->getCategoryTitle());?>"><?php echo $course->getCategoryTitle(); ?></a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="main-details mt10">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-7" >
					<div class="slp-promo-asset">
						<div class="promo-asset-content stretchy-wrapper ud-courseimpressiontracker" >

							<div class="asset-container">
								<video id="udemyVdo" class="video-js" controls preload="metadata" width="100%" height="100%" poster="<?php echo $course->getImage("750x422"); ?>" data-setup="{}">
									<?php foreach($course_detail->promo_asset->download_urls->Video as $video){ ?>
									<source src="<?php echo $video->file ?>" type="<?php echo $video->type ?>" data-res="<?php echo $video->label ?>">
									<?php } ?>
									<p class="vjs-no-js">
									  To view this video please enable JavaScript, and consider upgrading to a web browser that
									  <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
									</p>
								</video>
							</div>

						</div>
					</div>
				</div>
                    
				<div class="col-lg-4 col-md-5">
					<div class="row fxdc right-wrap mh0-force-md">
						<div class="fxw-md fdrr-md db-force-xs">
							<div class="right-top col-md-12 col-sm-6">
								<div class="take-btn">
									<div class="price fxac">
										<span class="current-price"><?php if($discount)echo $discount['text'];else echo $course->getPrice(); ?></span> 
										<?php if($discount){ ?>
											<div class="save-money dib ml10"><span class="old-price "><?php echo $course->getPrice(); ?></span></div>
											<span class="ml10 save-money"><?php echo $discount['percentage']; ?>% off</span>
										<?php }?>
									</div>
									<a href="<?php echo $course->getUdemyURL($UdemyDomainURL); ?>" class="btn btn-lg m10-0 course-cta btn-warning" onclick="_gaq.push(['_trackEvent', 'udemy','<?php echo $course->getCategoryTitle();?>','<?php echo $course->getUdemyURL('',false);?>']);">I'm interested</a>
								</div>
							</div>
							<div class="right-middle col-md-11 col-sm-6">
								<ul class="list">
									<li class="list-item">
										<span class="list-left">Lectures</span>
										<span class="list-right"><?php echo $course->getNumLectures(); ?></span>
									</li>
									<li class="list-item">
										<span class="list-left">Video</span>
										<span class="list-right"><?php echo $course->getContentInfo(); ?></span>
									</li>
									<li class="list-item">
										<span class="list-left">Skill Level</span>
										<span class="list-right"><?php echo $course->getInstructionalLvl(); ?></span>
									</li>

									<li class="list-item">
										<span class="list-left">Languages</span>
										<span class="list-right"><?php echo $course->getLanguage(); ?></span>
									</li>
								</ul>
							</div>
							<div class="clearfix visible-xs-block"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main-middle ud-angular-loader fs12-xs show-nav-on-this">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 pb50">
						<div id="desc" class="pos-r top-space-for-scroll">
							<h3 class="tab-title"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>
								Course Description
							</h3>
							<div class="hidedetail text-justify">
									<?php echo $course->getDescription(); ?>
							</div>
						</div>
						<div id="instructor" class="top-space-for-scroll" >
							<hr class="border-line">
							<div class="row" style='margin-bottom: 10px;'>
								<div class="col-sm-12">
									<h3 class="tab-title"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Instructor Biography</h3>
								</div>
								<?php foreach($course->getInstructors() as $instructor){ ?>
								<div class='row' style='margin: 5px;'>
									<div class="col-sm-4 col-md-3" style='margin-bottom: 10px;'>
										<a class="js-discover-tracker-elm" href="#">
											<img src="<?php echo $instructor->getImage("100x100"); ?>" width="140">
										</a>
										<div class="ins-info" >
											<a href="<?php echo $instructor->getURL($UdemyDomainURL); ?>" class="ins-name js-discover-tracker-elm"><?php echo $instructor->getDisplayName(); ?></a>,
											<?php echo $instructor->getJobTitle(); ?>
										</div>
									</div>
									<div class="col-sm-8 col-md-9 hidedetail text-justify">
										<?php echo $instructor->getDescription(); ?>
									</div>
								</div>
								<?php } ?>      
							</div>
							<hr class="border-line">
						</div>
						<div id="requirements" class="top-space-for-scroll">
							<h3 class="tab-title"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								What are the requirements?
							</h3>
							<ul class="regular-list">
								<?php foreach($course->getRequirements() as $item){ ?>
									<li><?php echo $item; ?></li>
								<?php } ?>
							</ul>
						</div>
						<div id="what-you-get" class="top-space-for-scroll">
							<h3 class="tab-title"><span class="glyphicon glyphicon-gift" aria-hidden="true"></span>
								What am I going to get from this course?
							</h3>
							<ul class="regular-list">
								<?php foreach($course->getObjectives() as $objective){ ?>
									<li><?php echo $objective; ?></li>
								<?php } ?>
							</ul>
						</div>
						<div id="who-should-attend" class="top-space-for-scroll">
							<h3 class="tab-title"><span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
								What is the target audience?
							</h3>
							<ul class="regular-list">
								<?php foreach($course->getTargetAudiences() as $audience){ ?>
									<li><?php echo $audience; ?></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
}
require_once ("_importjs.php");
?>
<script src="assets/js/video.js"></script>
<script src="assets/js/stjCollapsibleTxt.js"></script>
<!-- Fixed Scrolling -->
<script type="text/javascript">
$(window).scroll(function(){
	if ($(this).scrollTop() > 126) {
		$('#menu_slide').addClass('fixed navbar');
	} else {
		$('#menu_slide').removeClass('fixed navbar');
	}
});

$( ".hidedetail" ).each(function() {
	if(jQuery(this).height() > 190){
		jQuery(this).stjCollapsibleTxt({
			height: '190px',
			defaultHide: true,
			buttonContainerCls: 'text-center',
			button: {
				cls : "btnShowMore btn btn-info btn-xs",
				showtext : "Show More",
				hidetext : "Hide Details"
			},
		});
	}
});
</script>
<?php
require_once ("_footer.php");