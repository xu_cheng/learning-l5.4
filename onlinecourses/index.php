<!doctype html>
<html lang="en">
    <?php
    require_once ("_header.php");
    ?>
    <body class="controller-courses action-index white-bg">
        <?php
        require_once ("autoload.php");
        $arrCategory = getRandomCategoryKey(4);
        $courses = false;
        $criteria = new UdemyCriteria();
        $criteria->page = 1;
        $criteria->page_size = 5;
        $api = new UdemyApi();
        require_once("_searchbar.php");
        ?>
        <div class="main ud-courseimpressiontracker featured-courses">
            <div class="main-content fxw channels skin-search" >
                <div class="wrapper-right fx nobg-force-md p0-force-md">
                    <div class="carousel-fullscreen-sidebar" style="margin-top:0; padding-top:0px;">
                        <div id="discover-courses-rows" class="ud-courseimpressiontracker">
                            <?php
                            foreach ($arrCategory as $categoryKey) {
                                $criteria->category = $categoryKey;
                                $criteria->ordering = $MainPageUdemySort;
                                $courses = $api->search($criteria);
                                $categoryTtl = $UdemyCategories[$categoryKey];
                                $sortingTtl = $UdemySorting[$criteria->ordering];
                                include("_rowofcourses.php");
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        require_once ("_importjs.php");
        require_once ("_footer.php");
        ?>
    </body>
</html>  