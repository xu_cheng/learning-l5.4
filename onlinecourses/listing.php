<?php
$arrAdditionalStyle = array('');
require_once("_header.php");
require_once("autoload.php");
$courses = false;
$criteria = new UdemyCriteria();
if(isset($_GET ['page']))$criteria->page=$_GET ['page'];
if(isset($_GET ['search']))$criteria->search=$_GET ['search'];
if(isset($_GET ['category']))$criteria->category=$_GET ['category'];
if(isset($_GET ['subcategory']))$criteria->subcategory=$_GET ['subcategory'];
if(isset($_GET ['ordering']))$criteria->ordering=$_GET ['ordering'];
else $criteria->ordering = 'best_seller';
$criteria->page_size=$DefaultPageSize;
$api = new UdemyApi();
$courses = $api->search($criteria);
$courses = json_decode($courses);
require_once("_searchbar.php");
?>
<style>
.sortBySelect { display: none; }
@media (max-width: 767px) {
	.number-courses .coursefound { text-align: center;margin-bottom: 10px; }
	.number-courses .sort-by { text-align: center;width:100%; }
	.sortByLine { display: none; }
	.sortBySelect { display: inline-block; }
}
</style>
<div class="main-content fxw channels skin-search" >
	<div class="wrapper-right fx nobg-force-md p0-force-md">
		<div class="carousel-fullscreen-sidebar" style="margin-top:0; padding-top:0px;">
			<div id="discover-courses-rows" class="ud-courseimpressiontracker" data-tracking-type="mark-as-seen">
				<div class="ud-coursecarousel">
					<div class="number-courses">
						<div class="col-sm-4 coursefound"><h4><?php echo $courses->count; ?> courses found.</h4></div>
						<div class="col-sm-5 sort-by pull-right">
							<strong>Sort by:</strong>
							<span class='sortByLine'>
							<?php 
								$arrSortingHTML = array();
								$tmpCriteria = clone $criteria;
								$baseURL = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH).'?';
								foreach($UdemySorting as $key => $value){ 
									$tmpCriteria->ordering = $key;
									if($criteria->ordering == $key)$arrSortingHTML[] = "$value";
									else $arrSortingHTML[] = "<a title='$value' href='$baseURL".$tmpCriteria->getCriteriaQueryForDispaly()."' >$value</a>";
								}
								echo implode(' | ',$arrSortingHTML);
							?>
							</span>
							<span class='sortBySelect'>
								<select id='selSortBy'>
								<?php foreach($UdemySorting as $key => $value){ ?>
									<option value='<?php echo $key; ?>' <?php if($criteria->ordering == $key)echo 'selected'; ?> ><?php echo $value; ?></option>
								<?php } ?>
								</select>
							</span>
						</div>
						<div class="clearfix"></div>
					</div>
					<div>
						<center>
							<ul class="discover-courses-list channel-courses-list">
							<?php
							foreach($courses->results as $item){
								$course = new UdemyCourse($item);
								include('_UdemyCourseCard.php');
							}
							?>
							</ul>
						</center>
					</div>
					<div class="text-center">
						<?php if(ceil($courses->count/$criteria->page_size) >1){ ?>
						<ul id='pagination'></ul>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
require_once ("_importjs.php");
?>
<!--<script src="assets/js/jquery.bootpag.min.js"></script>-->
<script src="assets/js/bootstrap-paginator.js"></script>
<script type="text/javascript">
$(function() {
	$('#pagination').bootstrapPaginator({
		currentPage: <?php if($criteria->page)echo $criteria->page;else echo 1; ?>,
		totalPages: <?php echo ceil($courses->count/$criteria->page_size); ?>,
		numberOfPages:5,
		alignment:'center',
		bootstrapMajorVersion : 3,
		itemTexts: function (type, page, current) {
			switch (type) {
			case "first":
				return "<< First";
			case "prev":
				return "<<";
			case "next":
				return ">>";
			case "last":
				return "Last >>";
			case "page":
				return page;
			}
		},
		pageUrl: function(type, page, current){
			return updateURLParameter(window.location.href, 'page', page);
		}
	});
	
	jQuery('#selSortBy').on('change',function(){
		window.location = updateURLParameter(window.location.href, 'ordering', jQuery(this).val());
	});
});
</script>
<?php
require_once ("_footer.php");