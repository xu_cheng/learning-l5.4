<?php
Class UdemyApi{
	public $apiBaseURL = 'https://www.udemy.com/api-2.0/';
	public $timeout = 30;
	public $clientId = 'l5Oc4hx5CbiR43rWNwkM18mGWPjtqnp4IAQu7B9p';
	public $clientSecret = 'Nqv8mzOqfQGplnt9ZaL1XDyrV59tRbLPV4qAtNgZ7nCrd6r8XqGu5zrnzrxzU9efj3aHhcabGg6kjlnDlXhNxNd1ywqszJPJH6TnWOxK8AlM4kzXa5VByERtJYoCk14h';
	public $authorization = '';
	
	function __construct() {
       $this->authorization = base64_encode($this->clientId .':'.$this->clientSecret);
	}
	
	//return false on failure else return result json string
	private function request($url){
		$ch = curl_init( $this->apiBaseURL.$url);
		$headers = array('Authorization: Basic ' . $this->authorization );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => $headers ,
            CURLOPT_RETURNTRANSFER => 30
        );
        curl_setopt_array( $ch, $options );
		$result = curl_exec ($ch);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		//echo curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);exit();
		curl_close ($ch);
		if($status_code != 200)$result = false;
		return $result;
	}
	
	public function getCourseDetail($courseID){
		if(isset($courseID) && is_numeric($courseID)){
			$url = "courses/$courseID?country=SG&fields[course]=@all&fields[user]=@default,description";
			if($GLOBALS['UdemyPmTag'])$url .= "&pmtag=".$GLOBALS['UdemyPmTag'];
			return $this->request($url);
		}else return false;
	}
	
	public function search($objUdemyCriteria){
		$result = false;
		if(is_a($objUdemyCriteria,"UdemyCriteria")){
			$url = "courses/?".$objUdemyCriteria->getCriteriaQuery()."&fields[course]=title,price,price_detail,discount_price,num_lectures,content_info,visible_instructors,avg_rating,num_reviews,image_100x100,image_125_H,image_240x135,mage_480x270,primary_category";
			if($GLOBALS['UdemyPmTag'])$url .= "&pmtag=".$GLOBALS['UdemyPmTag'];
			$result = $this->request($url);
		}
		return $result;
	}
}