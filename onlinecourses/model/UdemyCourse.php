<?php
class  UdemyCourse{
	private $source;
	private $discount;
	
	function __construct($var) {
		if(!empty($var)){
			if(is_string($var))$this->source = json_decode($var);
			else if(is_object($var))$this->source = $var;
			
			if(isset($this->source->discount_price) && $this->source->discount_price){
				$originalPrice = 0;
				$discountedPercentage = 0;
				if(isset($this->source->price_detail->amount) && $this->source->price_detail->amount){
					$originalPrice = $this->source->price_detail->amount;
					$discountedPercentage = round((($originalPrice - $this->source->discount_price->amount)/$originalPrice) *100);
				}
				$this->discount = array( 
								"text" => $this->source->discount_price->price_string,
								"amount" => $this->source->discount_price->amount,
								"currency" => $this->source->discount_price->currency,
								"percentage" => $discountedPercentage
							);
			}
		}
	}
	
	public function getID(){
		if(isset($this->source->id) && $this->source->id)return $this->source->id;
		else return '';
	}
	
	public function getTitle(){
		if(isset($this->source->title) && $this->source->title)return $this->source->title;
		else return '';
	}
	
	public function getImage($size){
		$propName = "image_$size";
		if(isset($this->source->$propName) && $this->source->$propName)return $this->source->$propName;
		else return '';
	}
	
	public function getAvgRating(){
		if(isset($this->source->avg_rating) && $this->source->avg_rating)return $this->source->avg_rating*20;
		else return 0;
	}
	
	public function getNumReviews(){
		if(isset($this->source->num_reviews) && $this->source->num_reviews)return $this->source->num_reviews;
		else return 0;
	}
	
	public function getPrice(){
		if(isset($this->source->price) && $this->source->price)return $this->source->price;
		else return 0;
	}
	
	public function getDiscountInfo(){
		return $this->discount;
	}
	
	public function getNumLectures(){
		if(isset($this->source->num_lectures) && $this->source->num_lectures)return $this->source->num_lectures;
		else return 0;
	}
	
	public function getContentInfo(){
		if(isset($this->source->content_info) && $this->source->content_info)return $this->source->content_info;
		else return '';
	}
	
	public function getInstructors(){
		$arrResult = array();
		if(isset($this->source->visible_instructors) && $this->source->visible_instructors){
			foreach($this->source->visible_instructors as $instructor){
				$arrResult[] = new UdemyInstructor($instructor);
			}
		}
		return $arrResult;
	}
	
	public function getHeadline(){
		if(isset($this->source->headline) && $this->source->headline)return $this->source->headline;
		else '';
	}
	
	public function getNumSubscribers(){
		if(isset($this->source->num_subscribers) && $this->source->num_subscribers)return $this->source->num_subscribers;
		else return 0;
	}
	
	public function getCategoryTitle(){
		if(isset($this->source->primary_category->title) && $this->source->primary_category->title)return $this->source->primary_category->title;
		else return '';
	}
	
	public function getSubcategoryTitle(){
		if(isset($this->source->primary_subcategory->title) && $this->source->primary_subcategory->title)return $this->source->primary_subcategory->title;
		else return '';
	}
	
	public function getInstructionalLvl(){
		if(isset($this->source->instructional_level) && $this->source->instructional_level)return $this->source->instructional_level;
		else return '';
	}
	
	public function getDescription(){
		if(isset($this->source->description) && $this->source->description)return $this->source->description;
		else return '';
	}
	
	public function getRequirements(){
		if(isset($this->source->requirements_data->items) && $this->source->requirements_data->items)return $this->source->requirements_data->items;
		else return array();
	}
	
	public function getObjectives(){
		if(isset($this->source->objectives) && $this->source->objectives)return $this->source->objectives;
		else return array();
	}
	
	public function getTargetAudiences(){
		if(isset($this->source->target_audiences) && $this->source->target_audiences)return $this->source->target_audiences;
		else return array();
	}
	
	public function getUdemyURL($host = '',$includeTrackCode = true, $outboundalert = true){
		$url = $host.$this->source->url;
		if($includeTrackCode)$url .= '?'.$GLOBALS['UdemyUTMTrackingCode'];
		if($outboundalert)$url = $GLOBALS['OutboundAlertURL'].'?url='.urlencode($url);
		return $url;
	}
	
	public function getDetailPageURL(){
		return "detail.php?id=".$this->getID();
	}
	
	public function getLanguage(){
		if(isset($this->source->locale) && isset($this->source->locale->title) && $this->source->locale->title)
			return $this->source->locale->english_title;
		else return '';
	}
}