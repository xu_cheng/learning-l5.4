<?php
class  UdemyCriteria{
	public $page;
	public $page_size;
	public $is_paid;
	public $category;
	public $subcategory;
	public $search;
	public $language;
	public $ordering;
	public $is_marketing_boost_agreed;
	public $is_affiliate_agreed;
	public $is_fixed_priced_deals_agreed;
	public $is_percentage_deals_agreed;
	public $include_spam;
	public $country = 'SG';
	
	public function getCriteriaArray($arrExclude= array()){
		$arrProp = get_object_vars($this);
		foreach($arrProp as $key => $value){
			if(is_null($arrProp[$key]) || empty($arrProp[$key]) || in_array($key, $arrExclude))unset($arrProp[$key]);
		}
		return $arrProp;
	}
	
	public function getCriteriaQuery($arrExclude= array()){
		$arrCriteria = $this->getCriteriaArray($arrExclude);
		$strCriteria = '';
		foreach($arrCriteria as $key => $value){
			$strCriteria .= $key . '=' . urlencode($value) . '&';
		}
		if(strlen($strCriteria)!=0)$strCriteria = substr($strCriteria,0, -1);
		return $strCriteria;
	}
	
	public function getCriteriaQueryForDispaly(){
		return $this->getCriteriaQuery(array('page_size','country'));
	}
}