<?php
class  UdemyInstructor{
	private $source;
	
	function __construct($var) {
		if(!empty($var) && is_object($var)){
			$this->source = $var;
		}
	}
	
	public function getID(){
		return $this->source->id;
	}
	
	public function getImage($size){
		$propName = "image_$size";
		if(isset($this->source->$propName) && $this->source->$propName)return $this->source->$propName;
		else return '';
	}
	
	public function getDisplayName(){
		if(isset($this->source->display_name) && $this->source->display_name)return $this->source->display_name;
		else return '';
	}
	
	public function getJobTitle(){
		if(isset($this->source->job_title) && $this->source->job_title)return $this->source->job_title;
		else return '';
	}
	
	public function getURL($host){
		if(isset($this->source->url) && $this->source->url)return $host.$this->source->url;
		else return '';
	}
	
	public function getDescription(){
		if(isset($this->source->description) && $this->source->description)return $this->source->description;
		else return '';
	}
}