<?php
require_once ("autoload.php");
$pageTtl = 'Out Bound Alert';
require_once ("_header.php");
$destinationURL = '';
if(isset($_GET ['url']))$destinationURL = urldecode($_GET ['url']);
?>
<div class="row" style="margin: 10px;">
	<div class="col-md-4 center-block text-center" style="float:none;border:1px solid #bebebe;padding:10px;width:400px;">
		<h4>You will now be redirected to Udemy.com where you can learn more about this course.</h4>
		<div>
			Click <a href="<?php echo $destinationURL; ?>">here</a> to proceed or wait to be auto-redirected.
			<br>
			Go <a href="javascript: history.go(-1);">back</a> to the previous page.
		</div>
	</div>
</div>
<?php
require_once ("_importjs.php");
if($destinationURL){
?>
<script>
	setTimeout(function () {
		window.location.href = "<?php echo $destinationURL; ?>";
}, <?php echo $OutboundAlertDelay; ?>);
</script>
<?php
}
require_once ("_footer.php");