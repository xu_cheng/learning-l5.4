@extends('layouts.admin')

@section('content')
<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url("admin/banner/store"); ?>">
    {{ csrf_field() }}
    @include('banner.form')

    <div class="col-lg-4 col-md-4 col-sm-12">
        <label>Image (720 * 90) </label><input type='file' name='image1' accept="image/*" class='form-control' >
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <label>Image (320 * 50) </label><input type='file' name='image2' accept="image/*" class='form-control' >
    </div>
    <!-- 
       <div class="col-lg-3 col-md-3 col-sm-6">
           Banner Image 2 (Optional) : <input type='file' name='banner2' accept="image/*" class='form-control' >
       </div>
    -->

    <div style="margin: 20px" class="col-lg-12 col-sm-12">
        <button class="btn-lg btn-primary" type="submit">Create Banner</button>
    </div>
</form>
<script>
    $("form").submit(function () {
        $(this).submit(function () {
            return false;
        });
    });
</script>
@endsection
