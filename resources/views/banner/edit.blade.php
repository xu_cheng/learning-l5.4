@extends('layouts.admin')

@section('content')
<form class="form-horizontal" method="POST" action="<?php echo url("admin/banner/update/{$banner->id}"); ?>">
    {{ csrf_field() }}
    @include('banner.form')
    <hr>
    <div class="col-lg-12 col-md-12">
        <table class="table-striped table">
            <tr>
                <td><label>Image (720 * 90)</label></td>
                @if (empty($banner->image1))
                <td>Image not uploaded. </td>
                @else
                <td><img src="<?php echo url("/display/banner/{$banner->image1}"); ?>" alt="" height="90" weight="720"></td> 
                @endif
                <td><button type="button" class="btn btn-primary uploadBtn" data-toggle="modal" data-id="1">Upload file</button></td>
            </tr>
            <tr>
                <td><label>Image (320 * 50)</label></td>
                @if (empty($banner->image2))
                <td>Image not uploaded. </td>
                @else
                <td><img src="<?php echo url("/display/banner/{$banner->image2}"); ?>" alt="" height="50" weight="320"></td> 
                @endif
                <td><button type="button" class="btn btn-primary uploadBtn" data-toggle="modal" data-id="2">Upload file</button></td>
            </tr>
            
        </table>
    </div>
    <div style="margin: 20px" class="col-lg-12 col-md-12 col-sm-12">
        <button class="btn-lg btn-primary" type="submit">Update</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.uploadBtn').click(function () {
            id = $(this).data('id');
            $("#image_number").val(id);
            $('#uploadModal').modal('show');
        });
    });
</script>
@include('common.upload', ['item' => 'banner' , 'id' =>$banner->id ])
@endsection
