<div class="clearfix">
    <div class="col-lg-12 col-md-12">
        <label>URL</label>
        <input name="url" type="text" maxlength="255" class="form-control" placeholder="http://" value="{{ empty($banner->url) ? old('url') : $banner->url }}" >
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
        <label>Track Category</label>
        <input name="category" type="text" class="form-control" value="{{ empty($banner->category) ? old('category') : $banner->category }}" required>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6">
        <label>Track Label</label>
        <input name="label" type="text" class="form-control" value="{{ empty($banner->label) ? old('label') : $banner->label }}" required>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6">
        <label>Location</label>
        <select name="location" class="form-control" >
            <?php
            $location_list = config('constant.location');
            foreach ($location_list as $id => $location) {
                if (!empty($course->location) && $course->location == $id) {
                    echo "<option selected value='" . $id . "'>" . $location . "</option>";
                } else {
                    echo "<option value='" . $id . "'>" . $location . "</option>";
                }
            }
            ?>
        </select>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6">
        <label>Start Date</label>
        <input name="start_date" type="date" class="form-control" value="{{ empty($banner->start_date) ? date('Y-m-d') : $banner->start_date }}" >
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6">
        <label>End Date</label>
        <input name="end_date" type="date" class="form-control" value="{{ empty($banner->end_date) ? date('Y-m-d', strtotime('Dec 31'))  : $banner->end_date }}" >
    </div>
</div>