@extends('layouts.admin')

@section('content')

<table id="example" class="table table-striped" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Image</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $banners = App\Model\Banner::all();
        foreach ($banners as $banner) {
            ?>
            <tr>
                <td><img src="<?php echo url("/display/banner/{$banner->image1}"); ?>" alt="" height="90" weight="720"></td>
                <td><?php echo $banner->start_date; ?></td>
                <td><?php echo $banner->end_date; ?></td>
                <td>
                    <button type="button" class="btn-sm btn-warning" onclick="window.location.href = '<?php echo url("admin/banner/edit/{$banner->id}"); ?>'">Edit</button>
                    @if ($banner->isactive == 1)
                    <button data-id="{{$banner->id}}" type="button" class="btn-sm btn-danger deleteBtn" data-toggle="modal" >Deactivate</button>
                    @else
                    <button data-id="{{$banner->id}}" type="button" class="btn-sm btn-success" onclick="window.location.href = '<?php echo url("admin/banner/active/{$banner->id}"); ?>'">Activate</button>
                    @endif
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
@include('common.confirm_deactivate', ['item' => 'banner'])
@endsection