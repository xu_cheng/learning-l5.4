<?php
$now = date('Y-m-d');
$banners = App\Model\Banner::valid()->get();
?>
@if (count($banners) > 0)
<div class="row">                 
    <div class="owl-carousel banner-box" id="bn">
        <?php foreach ($banners as $banner) { ?>
            <div>
                <a href="{{$banner->url}}" target="_blank">
                    <div class="hidden-xs">
                        <img src="<?php echo url("/display/banner/{$banner->image1}"); ?>" class="img-responsive" onclick="ga('send', 'event', 'banner_{{$banner->category}}', 'click', '{{$banner->label}}');">
                    </div>
                    <div class="visible-xs">
                        <img src="<?php echo url("/display/banner/{$banner->image2}"); ?>" class="img-responsive" onclick="ga('send', 'event', 'banner_{{$banner->category}}', 'click', '{{$banner->label}}');">
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>
</div>
@endif