<!-- Modal -->
<div id="deleteConfirm" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirm Deactivate</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to deactivate this <?php echo $item; ?> ? </p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger delete" >Yes</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.deleteBtn').click(function () {
            var id = $(this).data('id');
            $('#deleteConfirm').data('id', id).modal('show');
        });
        $('.delete').click(function () {
            var id = $('#deleteConfirm').data('id');
            window.location.href = '<?php echo url("admin/". $item. "/delete/"); ?>/' + id;
        });
    });
</script>