<div id="contact" class="modal fade course-enquire" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Contact Us</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url("/enquiry"); ?>">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-xs-12 course-enquire-title"><?php echo $course->title; ?></div>
                        <input type="hidden" name="course" value="<?php echo $course->id; ?>">
                    </div>
                    @isset($course->provider)
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 course-enquire-label">Provided by: </div>
                        <div class="col-xs-12 col-sm-9"><?php echo $course->provider->name; ?></div>
                        <input type="hidden" name="provider" value="<?php echo $course->provider->id; ?>">
                    </div>
                    @endisset
                    <div class="form-group"> 
                        <label class="control-label col-sm-3">Nature of Enquiry:</label>

                        <div class="col-sm-9">
                            <div class="row">
                                <?php
                                $nature_list = config('constant.nature');
                                foreach ($nature_list as $nature) {
                                    echo "<div class='col-xs-12'><input type='checkbox' name='nature[]' value='" . $nature . "'>" . $nature . "</div>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="Name" >Name:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" placeholder="Enter Name" value="{{ old('name') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="Email">Email:</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" placeholder="Enter Email" value="{{ old('email') }}" required>
                        </div>
                    </div>

                    <!--                    <div class="form-group">
                                            <label class="control-label col-sm-3" for="Contact-Number">Contact Number:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="contact" placeholder="Enter Contact Number" value="{{ old('contact') }}" required>
                                            </div>
                                        </div>-->

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="Message">Message:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" rows="5" name="message" placeholder="Enter Message" required>{{ old('message') }}</textarea>
                        </div>
                    </div>                        
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <div class="g-recaptcha" data-sitekey="6LcnxV0UAAAAAOc4zpL0lOZA5YBHwyr9QSJBhkCS"></div>
                        </div>
                    </div>
                    <i>* Please note that the above information you have keyed in will be send to the training provider to follow up on your request. </i>

                    <div class="form-group"> 
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
