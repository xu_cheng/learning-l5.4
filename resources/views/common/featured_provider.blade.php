<?php $providers = App\Model\Provider::valid()->where('isfeature', 1)->get(); ?>
@if (count($providers) > 1)
<div class="row">
    <div class="col-xs-12 section-header">
        <h2><span>Featured Course Providers</span></h2>
    </div>

    <div class="col-xs-12">                    
        <div class="owl-carousel logo-slider" id="fcp">
            <?php
            foreach ($providers as $provider) {
                echo "<div>";
                if (empty($provider->url)) {
                    echo "<img src='" . url("/display/provider/" . $provider->logo) . "' alt='" . $provider->name . "' class='img-responsive'>";
                } else {
                    echo "<a target='_blank' href='" . $provider->url . "'><img src='" . url("/display/provider/" . $provider->logo) . "' alt='" . $provider->name . "' class='img-responsive'></a>";
                }
                echo "</div>";
            }
            ?>
        </div>
    </div>
</div>
@endif