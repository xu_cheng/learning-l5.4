<form method="get" id="search_form">
    <input type="text" id="keyword" value="{{ empty($keyword) ? "" : $keyword}}">
    <button id="search_btn">Search</button>
</form>

<script>
    $("#search_btn").on("click", function (e) {
        e.preventDefault();
        keyword = $.trim($("#keyword").val());
        if (keyword == '') {
            $('#search_form').attr('action', "<?php echo url('admin/'. $item . '/index'); ?>").submit();
        } else {
            $('#search_form').attr('action', "<?php echo url('admin/'. $item . '/index'); ?>/" + keyword).submit();
        }
    });
</script>