<form action="<?php echo url('/search'); ?>">
    <div class="row">
        <div class="col-xs-12 col-md-10 col-lg-10" style="margin-bottom:10px;">
            <?php
            $key = isset($_GET['search_key']) ? $_GET['search_key'] : null;
            ?>
            <input class="form-control site-search-input" name="search_key" type="text" placeholder="Search for Courses..." value="{{ empty(trim($key)) ? "" : $key }}">
        </div>
        <div class="col-xs-12 col-md-3 col-lg-3">
            <select name="course_type" class="form-control form-select" data-placeholder="Course Type">
                <option value=""></option>
                <?php
                if (isset($_GET['course_type'])) {
                    $type = $_GET['course_type'];
                    if ($type == "workshop") {
                        echo "<option value='workshop' selected>Workshops</option>";
                    } else {
                        echo "<option value='workshop'>Workshops</option>";
                    }
                    $sub_type_list = config('constant.subtype');
                    for ($i = 5; $i <= 5; $i ++) {
                        if ($type == $i) {
                            echo "<option selected value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        } else {
                            echo "<option value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        }
                    }
                    echo "<optgroup label='Higher Education'>";
                    for ($i = 1; $i <= 4; $i ++) {
                        if ($type == $i) {
                            echo "<option selected value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        } else {
                            echo "<option value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        }
                    }
                    echo "</optgroup>";
                } else {
                    ?> 
                    <option value="workshop">Workshops</option>
                    <option value="5">Skills Future</option>                        
                    <optgroup label="Higher Education">
                        <option value="1">Diploma</option>
                        <option value="3">Degree</option>
                        <option value="2">Certifications</option>
                        <option value="4">Masters/PhD</option>
                    </optgroup>
                <?php } ?>
            </select>
        </div>

        <div class="col-xs-12 col-md-3 col-lg-3">
            <select name="course_category" class="form-control form-select" id="category" data-placeholder="Category">
                <option value=""></option>
                <?php
                if (isset($_GET['course_category'])) {
                    $course_category = $_GET["course_category"];
                }
                $category_list = config('constant.category');
                foreach ($category_list as $id => $category) {
                    if (!empty($course_category) && $course_category == $id) {
                        echo "<option selected value='" . $id . "'>" . $category . "</option>";
                    } else {
                        echo "<option value='" . $id . "'>" . $category . "</option>";
                    }
                }
                ?>
            </select>
        </div>

        <div class="col-xs-12 col-md-3 col-lg-3">
            <select name="provider_id" class="form-control form-select" id="provider_id" data-placeholder="Provider">
                @forelse($providers as $p)
                    <option value=""></option>
                    @if(isset($_GET['provider_id']) && $p->id == $_GET['provider_id'])
                        <option selected value="{{ $p->id }}">{{ $p->name }}</option>
                    @else
                        <option value="{{ $p->id }}">{{ $p->name }}</option>
                    @endif
                @empty
                    <option value=""></option>
                    <option value="">Not Available</option>
                @endforelse
            </select>
        </div>

        <div class="col-xs-12 col-md-1 col-lg-1">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>