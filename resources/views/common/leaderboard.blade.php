<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
</script>

<script>
    googletag.cmd.push(function () {
        googletag.defineSlot('/5908/ST_jobs/lb1/learning', [[320, 50], [728, 90]], 'div-lb1-learning')
                 .addService(googletag.pubads())
                 .setTargeting('pos', ['1']);
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
    });
</script>

<div class="banner-box">  
    <!-- /5908/ST_jobs/lb1/learning -->
    <div id='div-lb1-learning'>
        <script>
            googletag.cmd.push(function () {
                googletag.display('div-lb1-learning');
            });
        </script>
    </div>
</div>

