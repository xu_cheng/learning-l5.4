<!-- NAVBAR SEARCH start -->
<div id="navbar-search" class="hidden-md hidden-lg hidden-sm collapse">            
    <div class="site-search-wrapper">
        @include('common.form.searchform')
    </div>    
</div>