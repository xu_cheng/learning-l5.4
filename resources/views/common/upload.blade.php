<!-- Modal -->
<div id="uploadModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Image upload</h4>
            </div>
            <div class="modal-body">
                <!-- Form -->
                <form method='post' action='<?php echo url('admin/image/edit/'); ?>' enctype="multipart/form-data">
                    {{ csrf_field() }}
                    Select file : <input type='file' name='file' id='file' accept="image/*" class='form-control' ><br>
                    <input type="hidden" name="item" value="{{$item}}">
                    <input type="hidden" name="id" value="{{$id}}">     
                    <input type="hidden" name="image_number" id="image_number" value="">
                    <input type='submit' class='btn btn-primary' value='Upload' id='upload'>
                </form>
            </div>
        </div>
    </div>
</div>