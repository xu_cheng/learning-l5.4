<div class="col-xs-12 section-header">
    <h2><span><?php echo $title; ?></span></h2>
</div>
<div class="col-xs-12 course-block">
    <div class="row grid">
        <?php foreach ($courses as $course) { ?>
            <div class="col-xs-12 col-sm-6 col-md-4 grid-item">
                <div class="course-single">
                    <a href="<?php echo url('/course/view/' . $course->id); ?>">
                        <?php if (empty($course->banner_one)) { ?>
                            <div class="course-photo"><img src="{{ asset('images/course-photo.jpg') }}" alt="" class="img-responsive"></div>
                        <?php } else { ?>
                            <div class="course-photo"><img src="<?php echo url("/display/course/{$course->banner_one}"); ?>" alt="" class="img-responsive"></div>
                        <?php } ?>
                        <div class="course-photo-hilite"></div>
                        <div class="course-content clearfix">
                            <h3><?php echo $course->title; ?></h3>
                            <?php foreach ($course->trainers as $trainer) { ?>
                                <div class="course-portrait pull-left">
                                    <img src="<?php echo url("/display/trainer/{$trainer->photo}"); ?>" alt="<?php echo $trainer->name; ?>" >
                                </div> 
                            <?php } ?>
                        </div>
                    </a>					
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div class="col-xs-12 text-center mb-60 mt-30">
    <a href="<?php echo url('/search?search_key=&course_category=&course_type=') . $type; ?>" class="btn-lg btn-more">Browse more courses</a>
</div>             