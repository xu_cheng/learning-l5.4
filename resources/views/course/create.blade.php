@extends('layouts.admin')

@section('content')
<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url("admin/course/store"); ?>">
    {{ csrf_field() }}
    @include('course.form')

    <div class="col-lg-4 col-md-4 col-sm-12">
        <label>Banner Image (360 * 240) </label><input type='file' name='banner1' accept="image/*" class='form-control' >
    </div>
    <!-- 
       <div class="col-lg-3 col-md-3 col-sm-6">
           Banner Image 2 (Optional) : <input type='file' name='banner2' accept="image/*" class='form-control' >
       </div>
    -->

    <div style="margin: 20px" class="col-lg-12 col-sm-12">
        <button class="btn-lg btn-primary" type="submit">Create Course</button>
    </div>
</form>
<script>
    $("form").submit(function () {
        $(this).submit(function () {
            return false;
        });
    });
</script>
@endsection
