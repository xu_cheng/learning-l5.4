@extends('layouts.admin')

@section('content')
<form class="form-horizontal" method="POST" action="<?php echo url("admin/course/update/{$course->id}"); ?>">
    {{ csrf_field() }}
    @include('course.form')

    <div class="col-lg-8 col-md-8">
        <table class="table-striped table">
            <tr>
                <td><label>Banner Image (360 * 240)</label></td>
                @if (empty($course->banner_one))
                <td>Image not uploaded. </td>
                @else
                <td><img src="<?php echo url("/display/course/{$course->banner_one}"); ?>" alt="" height="240" width="360"></td> 
                @endif
                <td><button type="button" class="btn btn-primary uploadBtn" data-toggle="modal" data-target="#uploadModal">Upload file</button></td>
            </tr>
        </table>     
    </div>
    <div style="margin: 20px" class="col-lg-12 col-md-12 col-sm-12">
        <button class="btn-lg btn-primary" type="submit">Update</button>
    </div>
</form>
@include('common.upload', ['item' => 'course' , 'id' =>$course->id ])
@endsection
