<div class="clearfix">
    <div class="col-lg-8 col-md-8">
        <label>* Course Title</label>
        <input name="title" type="text" maxlength="100" class="form-control" value="{{ empty($course->title) ? old('title') : $course->title }}" >
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6">
        <label>* Post Start Date</label>
        <input name="start_date" type="date" class="form-control" value="{{ empty($course->start_date) ? date('Y-m-d'): $course->start_date }}" >
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6">
        <label>* Post End Date</label>
        <input name="end_date" type="date" class="form-control" value="{{ empty($course->end_date) ? date('Y-m-d', strtotime('+60 days')) : $course->end_date }}" >
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <label>* Provider</label>
        <select name="provider" class="form-control" id="provider" >
            <?php
            echo "<option value=''></option>";
            foreach ($providers as $provider) {
                if (!empty($course->provider_id) && $course->provider_id == $provider->id) {
                    echo "<option selected value='" . $provider->id . "'>" . $provider->name . "</option>";
                } else {
                    echo "<option value='" . $provider->id . "'>" . $provider->name . "</option>";
                }
            }
            ?>
        </select>
    </div>
    <div  class="col-lg-4 col-md-4 col-sm-12">
        <label>Trainer</label>
        <select class="form-control" id="trainer" name="trainer[]" multiple="multiple" id="trainer">
            <?php
            foreach ($trainers as $trainer) {
                if (!empty($attached_trainers) && in_array($trainer->id, $attached_trainers)) {
                    echo "<option selected value=" . $trainer->id . " >" . $trainer->name . "</option>";
                } else {
                    echo "<option value=" . $trainer->id . " >" . $trainer->name . "</option>";
                }
            }
            ?>
        </select>
    </div>
</div>
<div class="clearfix">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <label>* Category</label>
        <select name="category" class="form-control" id="category">
            <?php
            $category_list = config('constant.category');
            foreach ($category_list as $id => $category) {
                if (!empty($course->category) && $course->category == $id) {
                    echo "<option selected value='" . $id . "'>" . $category . "</option>";
                } else {
                    echo "<option value='" . $id . "'>" . $category . "</option>";
                }
            }
            ?>
        </select>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12">
        <label>* Type</label>
        <select name="type" class="form-control" id="type" >
            <?php
            $type_list = config('constant.type');
            foreach ($type_list as $id => $type) {
                if (!empty($course->type) && $course->type == $id) {
                    echo "<option selected value='" . $id . "'>" . $type . "</option>";
                } else {
                    echo "<option value='" . $id . "'>" . $type . "</option>";
                }
            }
            ?>
        </select>
    </div>
    <div class="col-lg-4 col-md-4">
        <div id="sub_type_box">
            <?php if (!empty($course->type) && $course->type != 1) { ?> 
                <label>Sub Type</label><?php
                echo "<select name='sub_type' class='form-control'>";
                $sub_type_list = config('constant.subtype');
                if ($course->type == 2) {
                    for ($i = 1; $i <= 4; $i ++) {
                        if ($course->sub_type == $i) {
                            echo "<option selected value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        } else {
                            echo "<option value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        }
                    }
                }
                if ($course->type == 3) {
                    for ($i = 5; $i <= 5; $i ++) {
                        if ($course->sub_type == $i) {
                            echo "<option selected value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        } else {
                            echo "<option value='" . $i . "'>" . $sub_type_list[$i] . "</option>";
                        }
                    }
                }
                echo "</select>";
            }
            ?>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Fee Description (Before Discount/GST)</label>
            <input name="fee_before" type="text" class="form-control" value="{{ empty($course->fee_before) ? old('fee_before') : $course->fee_before }}">
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Fee Description (After Discount/GST)</label>
            <input name="fee_after" type="text" class="form-control" value="{{ empty($course->fee_after) ? old('fee_after') : $course->fee_after }}" >
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Fee Amount</label>
            <input name="fee" type="number" class="form-control" min="0" value="{{ empty($course->fee) ? old('fee') : $course->fee }}" >
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Registration Closing Date</label>
            <input name="clo_date" type="date" class="form-control" value="{{ empty($course->closing_date) ? old('clo_date') : $course->closing_date }}" >
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Course Duration</label>
            <input name="duration" type="text" class="form-control" value="{{ empty($course->duration) ? old('duration') : $course->duration }}" >
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Venue</label>
            <input name="venue" type="text" class="form-control" value="{{ empty($course->venue) ? old('venue') : $course->venue }}" >
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Date</label>
            <input name="date" type="date" class="form-control" value="{{ empty($course->date) ? old('date') : $course->date }}" >
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6">
            <label>Time</label>
            <input name="time" type="text" class="form-control" value="{{ empty($course->time) ? old('time') : $course->time }}" >
        </div>
        <div class="col-lg-8 col-md-8 col-sm-6">
            <div id="signupurl">
                <label>Sign Up Url</label>
                <input name="sign_up_url" placeholder="http://" type="text" class="form-control" value="{{ empty($course->sign_up_url) ? old('sign_up_url') : $course->sign_up_url }}" >
            </div>
        </div>
    </div>
    <hr>
    <div class="clearfix">
        <div class="col-lg-12 col-md-12">
            <label for="desc">Course Description</label>
            <textarea name="desc" class="form-control" id="desc" rows="10">{{ empty($course->desc) ? old('desc') : $course->desc }}</textarea>
        </div>
    </div>
    <hr/>
    <script src="{{ asset('js/admin_course.js') }}"></script>
    <script>
        CKEDITOR.replace('desc', {
        format_tags: 'p;h1;h2;h3;h4',
        });
    </script>