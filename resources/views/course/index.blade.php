@extends('layouts.admin')

@section('content')
@include('common.form.index_search', ['item' => 'course'])
<table id="example" class="table table-striped" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Course Title</th>
            <th>Provider</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($courses as $course) {
            ?>
            <tr>
                <td><?php echo $course->title; ?></td>
                <td><?php echo "<a href='". url("admin/provider/list/{$course->provider->id}"). "'>". $course->provider->name. "</a>"; ?></td>
                <td><?php
                    $type_list = config('constant.type');
                    $type = $type_list[$course->type];
                    echo $type;
                    ?>
                </td>
                <td>
                    <div class="button-group">
                        <button type="button" class="btn-sm btn-primary" onclick="window.open('<?php echo url("/course/view/{$course->id}"); ?>')">View</button>
                        <button type="button" class="btn-sm btn-warning" onclick="window.location.href = '<?php echo url("admin/course/edit/{$course->id}"); ?>'">Edit</button>
                        @if ($course->isactive == 1)
                        <button data-id="{{$course->id}}" type="button" class="btn-sm btn-danger deleteBtn" data-toggle="modal" >Deactivate</button>
                        @else
                        <button data-id="{{$course->id}}" type="button" class="btn-sm btn-success" onclick="window.location.href = '<?php echo url("admin/course/active/{$course->id}"); ?>'">Activate</button>
                        @endif
                    </div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div class='text-center'>
    {{ $courses->links() }}
</div>
@include('common.confirm_deactivate', ['item' => 'course'])
@endsection