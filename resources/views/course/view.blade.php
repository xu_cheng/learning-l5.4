@extends('layouts.master')

@section('content')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<!--preview script -->
<script type="text/javascript">
    function querystring(key) {
        var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
        var r = [], m;
        while ((m = re.exec(document.location.search)) != null)
            r[r.length] = m[1];
        return r;
    }
    var key = 'dfp_preview';
    var keycode = querystring(key);
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    
    gptadslots = [];
</script>
<script>
    googletag.cmd.push(function () {
        gptadslots[0] = googletag.defineSlot('/5908/ST_jobs/imu1/learning', [300, 250], 'dfp-ad-imu1')
                        .addService(googletag.pubads())
                        .setTargeting('pos', ['1']);
        gptadslots[1] = googletag.defineSlot('/5908/ST_jobs/lb1/learning', [[320, 50], [728, 90]], 'div-lb1-learning')
                        .addService(googletag.pubads())
                        .setTargeting('pos', ['1']);
        googletag.pubads().enableSingleRequest();
        googletag.pubads().setTargeting(key, keycode);
        googletag.enableServices();
        
    });
</script>
<div class="row view-course-top" >
    <div class="banner-box">  
        <div id='div-lb1-learning'>
            <script>
                googletag.cmd.push(function () {
                    googletag.display('div-lb1-learning');
                });
            </script>
        </div>
    </div>
</div>

@include('common.searchbar')
<div class="container">
    <section id="course_detail">
        <div class="row single-course-wrapper mb-60">
            <div class="col-md-8 col-sm-12">
                <div class="single-course-content">
                    <h3 class="course-title"><?php echo $course->title; ?></h3>
                    <div class="course-meta row">
                        <div class="col-sm-12 course-duration">
                            <div class="duration-details">
                                <span><?php //echo $course->time;                           ?></span>
                                <span><?php //echo $course->date;                           ?></span>
                            </div>
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </div>
                            @endif
                            @if (session('msg'))
                            <div class="alert alert-success">
                                {{ session('msg') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <!--
                    <div class="course-gallery">
                        <div class="course-photos owl-carousel">
                            <div class="item"><img src="{{ asset('images/course-details-photo-01.jpg') }}" alt="" class="img-responsive"></div>
                            <div class="item"><img src="{{ asset('images/course-details-photo-02.jpg') }}" alt="" class="img-responsive"></div>								
                        </div>
                    </div>
                    -->
                    <div class="course-info-tabs mt-30">
                        <div role="tabpanel">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tabe1" aria-controls="tabe1" role="tab" data-toggle="tab">Description</a>
                                </li>
                                @if (count($trainers) > 0)
                                <li role="presentation">
                                    <a href="#tab2" aria-controls="tab3" role="tab" data-toggle="tab">Trainer</a>
                                </li>
                                @endif
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade in" id="tabe1">
                                    @isset($course->desc)
                                    <?php echo $course->desc; ?>
                                    @endisset
                                </div>

                                @if (count($trainers) > 0)
                                <div role="tabpanel" class="tab-pane fade" id="tab2">
                                    <?php foreach ($trainers as $trainer) { ?>
                                        <div class="row">
                                            <div class="col-xs-12 course-trainer-info">                                                
                                                <div class="course-trainer">
                                                    <?php
                                                    if (empty($trainer->photo)) {
                                                        echo "<h2>" . $trainer->name . "</h2>";
                                                    } else {
                                                        ?>
                                                        <div class="course-trainer-photo"><img src="<?php echo url("/display/trainer/{$trainer->photo}"); ?>" alt="" class="img-responsive"></div>
                                                        <div class="course-trainer-name"><?php echo $trainer->name; ?></div>

                                                    <?php }
                                                    ?>
                                                </div>

                                                <h4>Trainer Description</h4>
                                                <p><?php echo $trainer->desc; ?></p>                                                
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="course-bottom-content">
                        <div class="course-tags">
                            <h4>Tags:</h4>
                            <ul class="list-unstyled">
                                <li><a href="">Wordpress</a></li>
                                <li><a href="">Campus</a></li>
                                <li><a href="">Class</a></li>
                                <li><a href="">Courses</a></li>
                                <li><a href="">Loop</a></li>
                                <li><a href="">Student</a></li>
                                <li><a href="">University</a></li>
                            </ul>
                        </div>
                    </div>
                    -->
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="single-course-sidebar">
                    <!--
                    <div class="course-share">
                        <h4>Share:</h4>
                        <ul class="list-unstyled">
                            <li><a href=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
                            <li><a href=""><i class="fa fa-whatsapp"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    -->
                    <div class="sidebar-widget">
                        <h3 class="widget-title">Course Features</h3>
                        <div class="widget-content course-features">
                            <div class="row">
                                <div class="col-sm-5 course-feature-label">Course Type:</div>
                                <div class="col-sm-7 course-feature-info"><?php
                                    $type_list = config('constant.type');
                                    echo $type_list[$course->type];
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5 course-feature-label">Category:</div>
                                <div class="col-sm-7 course-feature-info"><a href="<?php echo url('/search?search_key=&course_type=&course_category=') . $course->category; ?>">
                                        <?php
                                        $category_list = config('constant.category');
                                        echo $category_list[$course->category];
                                        ?></a></div>
                            </div>
                            @isset($course->date)
                            <div class="row">
                                <div class="col-sm-5 course-feature-label">Date:</div>
                                <div class="col-sm-7 course-feature-info"><?php echo date('d M Y', strtotime($course->date)) . "<br>" . $course->time; ?> </div>
                            </div>
                            @endisset

                            @isset($course->closing_date)
                            <div class="row">
                                <div class="col-sm-5 course-feature-label">Registration closing date:</div>
                                <div class="col-sm-7 course-feature-info"><?php echo date('d M Y', strtotime($course->closing_date)); ?></div>
                            </div>
                            @endisset

                            @isset($course->venue)
                            <div class="row">
                                <div class="col-sm-5 course-feature-label">Venue:</div>
                                <div class="col-sm-7 course-feature-info"><?php echo $course->venue; ?></div>
                            </div>
                            @endisset

                            @isset($course->duration)
                            <div class="row">
                                <div class="col-sm-5 course-feature-label">Duration:</div>
                                <div class="col-sm-7 course-feature-info"><?php echo $course->duration; ?></div>
                            </div>
                            @endisset

                            @isset($course->provider)
                            <div class="row">
                                <div class="col-sm-5 course-feature-label">Provider:</div>
                                <div class="col-sm-7 course-feature-info"><?php echo $course->provider->name; ?></div>
                            </div>
                            @endisset

                            <div class="course-fee">
                                @isset($course->fee)
                                <h4>Course Fee:</h4>
                                <p class="course-fee-after-gst"><?php echo $course->fee_after; ?></span></p>
                                <?Php if (!empty($course->fee_before)) { ?>
                                    <p class="course-fee-before-gst"><?php echo $course->fee_before; ?></span></p>
                                <?php } ?>
                                @endisset
                                <?Php if (!empty($course->sign_up_url)) { ?>
                                    <a href="{{$course->sign_up_url}}"><button type="button" class="course-contact" >Sign Up</button></a>
                                <?php } else { ?>
                                    <div><button type="button" class="course-contact" data-toggle="modal" data-target="#contact">Contact Us</button></div>
                                <?php } ?>
                            </div>
                            <div id='dfp-ad-imu1'>
                                <script>
                                    googletag.cmd.push(function () {
                                        googletag.display('dfp-ad-imu1');
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row">
        @include('common.featured_provider')
    </div>
</div>
@include('common.enquiry')

@endsection