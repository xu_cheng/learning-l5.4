<table width = '500' cellspacing = '2' cellpadding = '2' style = 'font-size:14px'><tbody>
        <tr>
            <p>You have an enquiry from a course listing in STJobs Learning portal. </p>
        </tr>
        <tr>
            <td colspan = '2' style = 'color:#254b90'><h4>{{ $nature }}</h4></td>
        </tr>
        <tr>
            <td><b>Course Title: </b></td><td>{{ $course }}</td>
        </tr>
        <tr>
            <td><b>Name: </b></td><td>{{ $name }}</td>
        </tr>
        <tr>
            <td><b>Email:</b></td><td><a href = 'mailto:{{ $email }}' target = '_blank'>{{ $email }}</a></td>
        </tr>
<!--        <tr>
            <td><b>Contact: </b></td><td> $contact </td>
        </tr>-->
        <tr>
            <td colspan = '2'> <hr style='border-top: dotted 1px;' /> </td>
        </tr>
        <tr>
            <td><b>Message: </b></td><td>{{ $msg }}</td>
        </tr>
    </tbody>
</table>