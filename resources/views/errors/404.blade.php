@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Error</div>

                <div class="panel-body">
                    The page is not found. Will redirect to <a href="<?php echo url('/'); ?>">homepage</a> in <b id="countdown">3</b> seconds... 
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // Update the count down every 1 second
    var x = setInterval(function () {
        // Output the result in an element with id="demo"
        var p = document.getElementById('countdown').textContent;
        var number = Number(p);
        number--;
        if (number == 0) {
            window.location.href = "<?php echo url('/'); ?>";
        } else {
            document.getElementById("countdown").textContent = number;
        }
    }, 1000);
</script>
@endsection


