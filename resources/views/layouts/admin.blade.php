<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>STJobs Learning</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/all.js') }}"></script>
        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset('css/select2.min.css') }}">
        <script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css">

    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">

                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            STJobs Learning
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @guest
                            @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
<!--                                    <li>
                                        <a href="{{ url('admin/password/change') }}">
                                            Change Password
                                        </a>
                                    </li>-->
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-3 col-md-2 sidebar">
                        <ul class="nav nav-sidebar">
                            <li class="active"><a href="{{url('admin/provider/index') }}">Provider </a></li>
                            <li><a href="{{url('admin/provider/create') }}">Add Provider</a></li>
                        </ul>
                        <ul class="nav nav-sidebar">
                            <li class="active"><a href="{{url('admin/course/index') }}">Course <span class="sr-only">(current)</span></a></li>
                            <li><a href="{{url('admin/course/create') }}">Add Course</a></li>
                        </ul>
                        <ul class="nav nav-sidebar">
                            <li class="active"><a href="{{url('admin/trainer/index') }}">Trainer </a></li>
                            <li><a href="{{url('admin/trainer/create') }}">Add Trainer</a></li>
                        </ul>
<!--                        <ul class="nav nav-sidebar">
                            <li class="active"><a href="{{url('admin/banner/index') }}">Banner </a></li>
                            <li><a href="{{url('admin/banner/create') }}">Add Banner</a></li>
                        </ul>-->
                        <ul class="nav nav-sidebar">
                            <li class="active"><a>Report </a></li>
                            <li><a href="{{url('admin/report/enquiry') }}">Download Enquiry</a></li>
                        </ul>
                        <ul class="nav nav-sidebar">
                            <li class="active"><a>Error</a></li>
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </div>
                            @endif
                        </ul>

                    </div>
                    <div class="col-sm-9 col-md-10"> 
                        @if (session('msg'))
                        <div class="alert alert-success">
                            {{ session('msg') }}
                        </div>
                        @endif
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
