<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="keywords" content="education, stjobs, learning">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>STJobs Learning, Find your right course today & further improve your career with STJobs Learning.</title>
        @if(env('APP_ENV') == 'production')
        @include('common.google')
        @endif
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- FontAwesome -->
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{ asset('css/carousel-main.css') }}" rel="stylesheet">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset('css/select2.min.css') }}">

        <link rel="stylesheet" href="{{ asset('css/style.css') }}">        
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom-responsive.css') }}">
    </head>
    <body>
        <!--==================
                Header
        ===================-->
        <header class="learning-nav">
            <nav class="navbar navbar-fixed-top">
                <div class="container">
                    <div class="hidden-sm">
                        <button type="button" class="navbar-toggle navbar-menu collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <button class="navbar-toggle navbar-search pull-right" type="button" data-toggle="collapse" data-target="#navbar-search">
                            <span class="sr-only">Toggle Search</span>                
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    <a class="navbar-brand" href="<?php echo url('/'); ?>" title="STJobs Learning"><img src="{{asset('/images/learning_logo_bt.jpg')}}" alt="STlearning"></a>
                    @include('common.mobile_search')

                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?php echo url('/'); ?>">Home</a></li>
                            <li><a href="{{ route('workshop') }}">Workshops</a></li>
                            <li class="dropdown">
                                <a href="{{ route('education') }}" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Higher Education<span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <li><a href="<?php echo url('search?search_key=&course_type=1&course_category='); ?>">Diploma</a></li>
                                    <li><a href="<?php echo url('search?search_key=&course_type=2&course_category='); ?>">Degree</a></li>
                                    <li><a href="<?php echo url('search?search_key=&course_type=3&course_category='); ?>">Certifications</a></li>
                                    <li><a href="<?php echo url('search?search_key=&course_type=4&course_category='); ?>">Masters/PhD</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="{{ route('training')}}" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">Training Courses<span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-menu-left">
                                    <li><a href="<?php echo url('/onlinecourses/'); ?>">Online Courses</a></li>
                                    <li><a href="<?php echo url('search?search_key=&course_type=5&course_category='); ?>">Skills Future</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ route('resource') }}">Resources</a></li>
                            <li><a href="http://www.scholarschoice.com.sg/" target="_blank">Scholarships</a></li>
                            <li><a href="http://www.stjobs.sg/info/contactus" target="_blank">Advertise</a></li>
                        </ul>

                    </div><!--/.nav-collapse -->
                </div>
            </nav>
        </header>

        <!-- ends: Header -->

        @yield('content')

        <footer class="site-footer">
            <div class="container">
                <div class="row site-footer-top">            
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <h4>Course Seekers</h4>
                        <ul>
                            <li><a href="{{ route('workshop') }}">Workshops</a></li>
                            <li><a href="{{ route('education') }}">Higher Education Courses</a></li>
                            <li><a href="<?php echo url('/onlinecourses/'); ?>">Online Courses</a></li>
                            <li><a href="<?php echo url('search?search_key=&course_type=5&course_category='); ?>">SkillsFuture Courses</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <h4>Providers</h4>
                        <ul>
                            <li><a href="http://www.stjobs.sg/info/contactus" target="_blank">Advertise With Us</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <h4>Contact Us</h4>                           
                        <ul>
                            <li class="site-footer-contact"><i class="fa fa-phone"></i>6319 8877</li>
                            <li class="site-footer-contact"><i class="fa fa-envelope"></i><a href="mailto:customercare@stjobs.sg" target="_blank">customercare@stjobs.sg</a></li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <h4>Follow Us</h4>                           
                        <ul>                            
                            <li class="site-footer-social"><a href="http://www.facebook.com/STJobs.SG" target="_blank"><i class="fa fa-facebook"></i></a> <a href="http://instagram.com/stjobssg" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div><!--/.row-->

                <div class="row site-footer-bottom">
                    <div class="col-xs-12 col-md-6 col-lg-7 site-footer-branding">
                        <div class="site-footer-branding-inner">
                            <div class="site-footer-branding-logo"><a href="http://www.stjobs.sg" target="_blank"><img src="{{ asset('images/logo-footer.png') }}" class="img-responsive" alt="STJobs"/></a></div>
                            <div class="site-footer-branding-info">
                                <p>&copy; 2017 Singapore Press Holdings Ltd.</p>                            
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-lg-5 site-footer-terms">
<!--                        <a href="#">About Us</a><span>|</span>-->
                        <a href="http://www.stjobs.sg/info/terms" target="_blank">Terms &amp; Conditions</a><span>|</span>
                        <a href="http://sph.com.sg/legal/sph_privacy.html" target="_blank">Privacy Policy</a>
                    </div>

                    <p id="back-top"><a href="#top"><i class="fa fa-angle-up"></i></a></p>
                </div><!--/.row-->
            </div><!--/.container-->
        </footer>


        <!--==================
                JS Files
        ===================-->
        <!-- jQuery -->
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/all.js') }}"></script>
        <script src="https://adtag.sphdigital.com/tag/stjobs/js/stjobs_dmp_basic.js" ></script>
        <script>
/* Isotope for listings grid layout */
var $grid = $('.grid').imagesLoaded(function () {
    // init Isotope after all images have loaded
    $grid.isotope({
        // options
        itemSelector: '.grid-item',
        layoutMode: 'fitRows' //can set value to 'masonry'
    });
});

var owlCarouselOpt = {
    autoplayTimeout: 5000, // Sets AutoPlay interval 
    loop: true,
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 500,
    dots: false,
    nav: true,
    slideBy: 'page',
    navText: [
        "<i class='fa fa-chevron-left'></i>",
        "<i class='fa fa-chevron-right'></i>"
    ],
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            autoplayTimeout: 2000
        },
        700: {
            items: 3,
            autoplayTimeout: 3000
        },
        1024: {
            items: 5,
            autoplayTimeout: 5000
        },
    }
};
$(document).ready(function () {
    $("#fcp").owlCarousel(owlCarouselOpt);
    /* SELECT2 */
    $('.form-select').select2({width: 'resolve'});
    $('#bn').owlCarousel({
        autoplayTimeout: 6000,
        loop: true,
        margin: 10,
        nav: false,
        items: 1,
        dots: false,
        autoplay: true,
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});
</script>

    </body>
</html>
