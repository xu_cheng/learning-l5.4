@extends('layouts.master')

@section('content')
<section class="page-header" style="background-image: url('images/home-search.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Higher Education</h1>					
            </div>
        </div>
    </div>
</section>

@include('common.searchbar')

<div class="container">
    @include('common.leaderboard')
    <div class="all-course-wrapper course">
        @if (count($diploma_course) > 0)
        <section id="diploma_course" class="row">
            @include('common.course', ['title' => 'Recommended Diploma Courses' , 'type' =>'1' , 'courses' => $diploma_course])
        </section>
        @endif   

        @if (count($degree_course) > 0)
        <section id="degree_course" class="row">
            @include('common.course', ['title' => 'Recommended Degree Courses' , 'type' =>'2' , 'courses' => $degree_course])
        </section>
        @endif

        @if (count($certifications_course) > 0)
        <section id="certifications_course" class="row">
            @include('common.course', ['title' => 'Recommended Certifications Courses' , 'type' =>'3' , 'courses' => $certifications_course])
        </section>
        @endif

        @if (count($master_course) > 0)
        <div id="master_course" class="row">
            @include('common.course', ['title' => 'Recommended Masters/PhD Courses' , 'type' =>'4' , 'courses' => $master_course])
        </div>
        @endif

        @include('common.featured_provider')

    </div>
</div>
@endsection