@extends('layouts.master')

@section('content')
<section class="home" style="background-image: url('images/home-search.jpg');">
    <div class="home-content">
        <div class="container">
            <h1>Take your first step to lifelong learning</h1>
            <form action="<?php echo url('/search'); ?>" class="home-search hidden-xs">
                <div class="form-group clearfix">
                    <input type="text" placeholder="What do you want to learn today?" name="search_key">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </div>
                <div class="form-group clearfix">
                    <select name="course_type" class="form-select" id="type" data-placeholder="Course Type" style="width: 26%">
                        <option value=""></option>
                        <option value="workshop">Workshops</option>
                        <option value="5">Skills Future</option>                        
                        <optgroup label="Higher Education">
                            <option value="1">Diploma</option>
                            <option value="2">Degree</option>
                            <option value="3">Certifications</option>
                            <option value="4">Masters/PhD</option>
                        </optgroup>
                    </select>
                    <select name="course_category" class="form-select" id="category" data-placeholder="Category" style="width: 26%">
                        <option value=""></option>
                        <?php
                        $category_list = config('constant.category');
                        foreach ($category_list as $id => $category) {
                            if (!empty($course->category) && $course->category == $id) {
                                echo "<option selected value='" . $id . "'>" . $category . "</option>";
                            } else {
                                echo "<option value='" . $id . "'>" . $category . "</option>";
                            }
                        }
                        ?>
                    </select>
                    <select name="provider_id" class="form-select" id="provider_id" data-placeholder="Provider" style="width: 26%">
                        @forelse($providers as $p)
                            <option value=""></option>
                            <option value="{{ $p->id }}">{{ $p->name }}</option>
                        @empty
                            <option value=""></option>
                            <option value="">Not Available</option>
                        @endforelse
                    </select>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="container">
    @include('common.leaderboard')

    <div class="course">
        @if (count($recommended_course) > 0)
        <section id="recommended_course" class="row">
            @include('common.course', ['title' => 'Recommended Courses' , 'type' =>'courses' , 'courses' => $recommended_course])
        </section>
        @endif

        @if (count($upcoming_workshop) > 0)
        <section id="upcoming_workshop" class="row">
            @include('common.workshop', ['title' => 'Upcoming Workshops' , 'type' =>'workshop' , 'courses' => $upcoming_workshop])
        </section>
        @endif
        
        @include('common.featured_provider')
    </div>
</div>
@endsection