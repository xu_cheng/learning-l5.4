@extends('layouts.master')

@section('content')
<section class="page-header" style="background-image: url('images/home-search.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Resources</h1>
            </div>
        </div>
    </div>
</section>

@include('common.searchbar')

<div class="container">
    @include('common.leaderboard')
    <div class="row all-course-wrapper course">  
        <section id="workshop">
            <div class="col-xs-12">
                <div class="course-block">
                    <div class="row grid">
                        <?php foreach ($articles as $article) { ?>
                            <div class="col-xs-12 col-sm-6 col-md-4 grid-item">
                                <div class="course-single">
                                    <a target="_blank" href="<?php echo $article->url; ?>">
                                        <?php if (empty($article->photo)) { ?>
                                            <div class="course-photo"><img src="{{ asset('images/course-photo.jpg') }}" alt="" class="img-responsive"></div>
                                        <?php } else { ?>
                                            <div class="course-photo"><img src="<?php echo str_replace( 'http://', 'https://', $article->photo ); ?>" alt="" class="img-responsive"></div>
                                        <?php } ?>
                                        <div class="course-photo-hilite"></div>
                                        <div class="course-content clearfix">
                                            <h3><?php echo $article->title; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class='text-center'>
        <?php echo $pagin; ?>
    </div>
    
</div>
@endsection