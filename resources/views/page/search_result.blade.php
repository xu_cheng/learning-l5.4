@extends('layouts.master')

@section('content')
<div class="row view-course-top" >
    @include('common.leaderboard')
</div>
@include('common.searchbar')

<div class="container">
    <div class="row all-course-wrapper course">  
        <section id="workshop">
            <div class="col-xs-12">
                <div class="course-block">
                    <div class="row grid">
                        <?php foreach ($courses as $course) { ?>
                            <div class="col-xs-12 col-sm-6 col-md-4 grid-item">
                                <div class="course-single">
                                    <a href="<?php echo url('/course/view/' . $course->id); ?>">
                                        <?php if (empty($course->banner_one)) { ?>
                                            <div class="course-photo"><img src="{{ asset('images/course-photo.jpg') }}" alt="" class="img-responsive"></div>
                                        <?php } else { ?>
                                            <div class="course-photo"><img src="<?php echo url("/display/course/{$course->banner_one}"); ?>" alt="" class="img-responsive"></div>
                                        <?php } ?>
                                        <div class="course-photo-hilite"></div>
                                        <div class="course-content clearfix">
                                            <h3><?php echo $course->title; ?></h3>
                                            <?php if ($course->type == 1) { ?>
                                                <?php foreach ($course->trainers as $trainer) { ?>
                                                    <div class="course-portrait pull-left">
                                                        <img src="<?php echo url("/display/trainer/{$trainer->photo}"); ?>" alt="<?php echo $trainer->name; ?>" >
                                                    </div> 
                                                    <?php
                                                }
                                            } else {
                                                if (!empty($course->provider_id)) {
                                                    ?>
                                                    <div class = "course-portrait pull-left">
                                                        <img src = "<?php echo url("/display/provider/{$course->provider->logo}"); ?>" alt = "<?php echo $course->provider->name; ?>" >
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class='text-center'>
        {{ $courses->links() }}
    </div>
</div>@endsection
