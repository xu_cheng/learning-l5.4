@extends('layouts.master')

@section('content')

<section class="page-header" style="background-image: url('images/home-search.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Training Courses</h1>					
            </div>
        </div>
    </div>
</section>

@include('common.searchbar')

<div class="container">
    @include('common.leaderboard')
    <div class="all-course-wrapper course">
<!--        @if (count($online_course) > 0)
        <section id="online_course" class="row">
            @include('common.course', ['title' => 'Recommended Online Courses' , 'type' =>'5' , 'courses' => $online_course])
        </section>
        @endif -->

        @if (count($skillfuture_course) > 0)
        <section id="skillfuture_course" class="row">
            @include('common.course', ['title' => 'Recommended SkillsFuture Courses' , 'type' =>'5' , 'courses' => $skillfuture_course])
        </section>   
        @endif 
    </div>
</div>

@endsection