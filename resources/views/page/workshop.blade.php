@extends('layouts.master')

@section('content')
<section class="page-header" style="background-image: url('images/home-search.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Workshops</h1>
            </div>
        </div>
    </div>
</section>

@include('common.searchbar')

<div class="container">
    @include('common.leaderboard')
    <div class="row all-course-wrapper course">  
        <section id="workshop">
            <div class="col-xs-12">
                <div class="course-block">
                    <div class="row grid">
                        <?php foreach ($courses as $course) { ?>
                            <div class="col-xs-12 col-sm-6 col-md-4 grid-item">
                                <div class="course-single">
                                    <a href="<?php echo url('/course/view/' . $course->id); ?>">
                                        <?php if (empty($course->banner_one)) { ?>
                                            <div class="course-photo"><img src="{{ asset('images/course-photo.jpg') }}" alt="" class="img-responsive"></div>
                                        <?php } else { ?>
                                            <div class="course-photo"><img src="<?php echo url("/display/course/{$course->banner_one}"); ?>" alt="" class="img-responsive"></div>
                                        <?php } ?>
                                        <div class="course-photo-hilite"></div>
                                        <div class="course-content clearfix">
                                            <h3><?php echo $course->title; ?></h3>
                                            <?php foreach ($course->trainers as $trainer) { ?>
                                                <div class="course-portrait pull-left">
                                                    <img src="<?php echo url("/display/trainer/{$trainer->photo}"); ?>" alt="<?php echo $trainer->name; ?>" >
                                                </div> 
                                            <?php } ?>	
                                        </div>
                                    </a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class='text-center'>
        {{ $courses->links() }}
    </div>
</div>
@endsection