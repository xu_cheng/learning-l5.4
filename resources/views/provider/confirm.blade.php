@extends('layouts.app')

@section('content')
<?php
if ($provider->isactive == 1) {
    $action = "deactivate";
    $link = "delete";
} else {
    $action = "activate";
    $link = "active";
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Confirm <mark><?php echo ucfirst($action); ?></mark> Provider</h4></div>

                <div class="panel-body">
                    <p>Are you sure you want to <?php echo $action ?> provider <b><?php echo $provider->name; ?></b>? </p>
                    <hr>
                    <form method="GET" action="<?php echo url("admin/provider/". $link . "/" . $provider->id); ?>">
                        {{ csrf_field() }}
                        <input type="checkbox" name="courses"> Also <?php echo $action ?> all courses under this provider. <br>
                        <p class="text-danger"> There are <?php echo $provider->admin_courses()->count(); ?> courses belongs to this provider. </p>
                        <button type="submit" class="btn btn-danger" >Yes</button>
                        <button type="button" class="btn btn-warning" onclick="window.location.href = '<?php echo url("admin/provider/index"); ?>'">No</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection