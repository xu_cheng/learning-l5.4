@extends('layouts.admin')

@section('content')
<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url("admin/provider/store"); ?>">
    {{ csrf_field() }}
    @include('provider.form')
    <hr>
    <div class="col-lg-6 col-md-6">
        <label>Logo (80 * 50) </label><input type='file' name='logo' accept="image/*" class='form-control' >
    </div>
    <br>
    <div style="margin: 20px" class="col-lg-6 col-sm-6">
        <button class="btn-lg btn-primary" type="submit">Create Provider</button>
    </div>
</form>
@endsection

