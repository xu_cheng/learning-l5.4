@extends('layouts.admin')

@section('content')

<form class="form-horizontal" method="POST" action="<?php echo url("admin/provider/update/{$provider->id}"); ?>">
    {{ csrf_field() }}
    @include('provider.form')
    <hr/>
    <div  class="col-lg-8 col-md-12 col-sm-12">
        <table style="width:100%" class="table table-striped">
            <tr>
                <td><label>Logo (80 * 50) </label></td>
                <td><img src="<?php echo url("/display/provider/{$provider->logo}"); ?>" alt="" height="50" ></td> 
                <td><button type="button" class="btn btn-primary uploadBtn" data-toggle="modal" data-target="#uploadModal">Upload</button></td>
            </tr>
        </table>     
    </div>
    <hr>
    <div style="margin: 20px" class="col-lg-6 col-sm-6">
        <button class="btn-lg btn-primary" type="submit">Update</button>
    </div>
</form>
@include('common.upload', ['item' => 'provider' , 'id' =>$provider->id ])
@endsection
