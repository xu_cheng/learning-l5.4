<div class="clearfix">
    <div class="col-lg-8 col-md-8">
        <label>* Company Name</label>
        <input name="name" type="text" maxlength="100" class="form-control" value="{{ empty($provider->name) ? old('name') : $provider->name }}" >
    </div>
    <div class="col-lg-4 col-md-4">
        <label>* Email</label>
        <input name="email" type="email" maxlength="100" class="form-control" value="{{ empty($provider->email) ? old('email') : $provider->email }}" >
    </div>
    <div class="col-lg-6 col-md-6">
        <label>Address</label>
        <input name="address1" type="text" maxlength="100" class="form-control" value="{{ empty($provider->address1) ? old('address1') : $provider->address1 }}" >
        <input name="address2" type="text" maxlength="100" class="form-control" value="{{ empty($provider->address2) ? old('address2') : $provider->address2 }}" >
        <input name="address3" type="text" maxlength="100" class="form-control" value="{{ empty($provider->address3) ? old('address3') : $provider->address3 }}" >
    </div>
    <div class="col-lg-3 col-md-3">
        <label>Postcode</label>
        <input name="postcode" type="number" min="0" class="form-control" value="{{ empty($provider->postcode) ? old('postcode') : $provider->postcode }}" >
    </div>
    <div class="col-lg-3 col-md-3">
        <label>Contact No</label>
        <input name="contact" type="text" maxlength="100" class="form-control" value="{{ empty($provider->contact) ? old('contact') : $provider->contact }}" >
    </div>
    <div class="col-lg-3 col-md-3">
        <label>* Is featured provider</label>
        <div class="radio">
            <?php if (!empty($provider->isfeature) && $provider->isfeature == 1) { ?>
                <label><input type="radio" name="isfeature" value="1" checked>Yes</label>
                <label><input type="radio" name="isfeature" value="0" >No</label>
            <?php } else { ?>
                <label><input type="radio" name="isfeature" value="1" >Yes</label>
                <label><input type="radio" name="isfeature" value="0" checked>No</label>
            <?php } ?>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <label>* URL</label>
        <input name="url" type="text" maxlength="250" placeholder="http://" class="form-control" value="{{ empty($provider->url) ? old('url') : $provider->url }}" >
    </div>
</div>

<hr>
<div class="clearfix">
    <div class="col-lg-12 col-md-12">
        <label>Description</label>
        <textarea name="desc" class="form-control" rows="6" >{{ empty($provider->desc) ? old('desc') : $provider->desc }}</textarea>
    </div>
</div>
<script>
    CKEDITOR.replace('desc', {
        format_tags: 'p;h1;h2;h3;h4',
    });
</script>