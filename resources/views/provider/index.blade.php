@extends('layouts.admin')

@section('content')

@include('common.form.index_search', ['item' => 'provider'])
<table id="example" class="table table-striped" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Company Name</th>
            <th>Email</th>
            <th>Logo</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($providers as $provider) {
            ?>
            <tr>
                <td><?php echo "<a href='" . url("admin/provider/list/{$provider->id}") . "'>" . $provider->name . "</a>"; ?></td>
                <td><?php echo $provider->email; ?></td>
                <td><img src="<?php echo url("/display/provider/{$provider->logo}"); ?>" alt="" height="50"></td> 
                <td>
                    <!--
                    <button disabled type="button" class="btn-sm btn-primary" onclick="window.open('<?php echo url("/provider/view/{$provider->id}"); ?>')">View</button>
                    -->
                    <button type="button" class="btn-sm btn-warning" onclick="window.location.href = '<?php echo url("admin/provider/edit/{$provider->id}"); ?>'">Edit</button>
                    @if ($provider->isactive == 1)
                    <button type="button" class="btn-sm btn-danger" onclick="window.location.href = '<?php echo url("admin/provider/confirm/{$provider->id}"); ?>'">Deactivate</button>
                    @else
                    <button type="button" class="btn-sm btn-success" onclick="window.location.href = '<?php echo url("admin/provider/confirm/{$provider->id}"); ?>'">Activate</button>
                    @endif
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div class='text-center'>
    {{ $providers->links() }}
</div>
@endsection