@extends('layouts.admin')

@section('content')
<h4>{{$provider->name}} </h4>
@include('common.form.list_search', ['item' => 'provider', 'id' => $provider->id])

<table id="example" class="table table-striped" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Course Title</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($courses as $course) {
            ?>
            <tr>
                <td><?php echo $course->title; ?></td>
                <td><?php echo $course->start_date; ?></td>
                <td><?php echo $course->end_date; ?></td>
                <td>
                    <div class="button-group">
                        <button type="button" class="btn-sm btn-primary" onclick="window.open('<?php echo url("/course/view/{$course->id}"); ?>')">View</button>
                        <button type="button" class="btn-sm btn-warning" onclick="window.location.href = '<?php echo url("admin/course/edit/{$course->id}"); ?>'">Edit</button>
                        @if ($course->isactive == 1)
                        <button data-id="{{$course->id}}" type="button" class="btn-sm btn-danger deleteBtn" data-toggle="modal" >Deactivate</button>
                        @else
                        <button data-id="{{$course->id}}" type="button" class="btn-sm btn-success" onclick="window.location.href = '<?php echo url("admin/course/active/{$course->id}"); ?>'">Activate</button>
                        @endif
                    </div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<div class='text-center'>
    {{ $courses->links() }}
</div>
@include('common.confirm_deactivate', ['item' => 'course'])
@endsection