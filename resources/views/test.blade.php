@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <form action="<?php echo route('test'); ?>" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    Select image to upload:
                    <input type="file" name="file" id="fileToUpload">
                    <input type="submit" value="Upload Image" name="submit">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
