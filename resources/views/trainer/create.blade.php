@extends('layouts.admin')

@section('content')
<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="<?php echo url("admin/trainer/store"); ?>">
    {{ csrf_field() }}
    @include('trainer.form')
    <hr>
    <div class="col-lg-6 col-md-6">
        <label>Photo (150 * 150)</label> <input type='file' name='photo' accept="image/*" class='form-control' >
    </div>
    <br>
    <div style="margin: 20px" class="col-lg-6 col-sm-6">
        <button class="btn-lg btn-primary" type="submit">Create Trainer</button>
    </div>
</form>
@endsection

