@extends('layouts.admin')

@section('content')

<form class="form-horizontal" method="POST" action="<?php echo url("admin/trainer/update/{$trainer->id}"); ?>">
    {{ csrf_field() }}
    @include('trainer.form')
    <hr/>
    <div  class="col-lg-8 col-md-12 col-sm-12">
        <table class="table table-striped">
            <tr>
                <td><label>Photo (150 * 150)</label></td>
                @if (empty($trainer->photo))
                <td>Image not uploaded. </td>
                @else
                <td><img src="<?php echo url("/display/trainer/{$trainer->photo}"); ?>" alt="" height="150" width="150"></td> 
                @endif
                <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload</button></td>
            </tr>
        </table>     
    </div>
    <hr>
    <div style="margin: 20px" class="col-lg-6 col-sm-6">
        <button class="btn-lg btn-primary" type="submit">Update</button>
    </div>
</form>
@include('common.upload', ['item' => 'trainer' , 'id' =>$trainer->id ])
@endsection
