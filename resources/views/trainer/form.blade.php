<div class="clearfix">
    <div class="col-lg-8 col-md-8">
        <label>Name</label>
        <input name="name" type="text" maxlength="100" class="form-control" value="{{ empty($trainer->name) ? old('name') : $trainer->name }}" >
    </div>
</div>
<hr>
<div class="clearfix">
    <div class="col-lg-8 col-md-8">
        <label>Description</label>
        <textarea name="desc" class="form-control" rows="6" >{{ empty($trainer->desc) ? old('desc') : $trainer->desc }}</textarea>
    </div>
</div>
<script>
    CKEDITOR.replace('desc', {
        format_tags: 'p;h1;h2;h3;h4',
    });
</script>