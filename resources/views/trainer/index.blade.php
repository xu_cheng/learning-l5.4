@extends('layouts.admin')

@section('content')

@include('common.form.index_search', ['item' => 'trainer'])
<table id="example" class="table table-striped" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Photo</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($trainers as $trainer) {
            ?>
            <tr>
                <td><?php echo $trainer->name; ?></td>
                @if (empty($trainer->photo))
                <td>Image not uploaded. </td>
                @else
                <td><img src="<?php echo url("/display/trainer/{$trainer->photo}"); ?>" alt="" height="150" width="150"></td> 
                @endif
                <td>
                    <!--
                    <button disabled type="button" class="btn-sm btn-primary" onclick="window.open('<?php echo url("/trainer/view/{$trainer->id}"); ?>')">View</button>
                    -->
                    <button type="button" class="btn-sm btn-warning" onclick="window.location.href = '<?php echo url("admin/trainer/edit/{$trainer->id}"); ?>'">Edit</button>
                    @if ($trainer->isactive == 1)
                    <button data-id="{{$trainer->id}}" type="button" class="btn-sm btn-danger deleteBtn" data-toggle="modal" >Deactivate</button>
                    @else
                    <button data-id="{{$trainer->id}}" type="button" class="btn-sm btn-success" onclick="window.location.href = '<?php echo url("admin/trainer/active/{$trainer->id}"); ?>'">Activate</button>
                    @endif
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<div class='text-center'>
    {{ $trainers->links() }}
</div>
@include('common.confirm_deactivate', ['item' => 'trainer'])
@endsection