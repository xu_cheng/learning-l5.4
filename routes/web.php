<?php

//Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LdapController@login');
Route::get('logout', function() {
    return abort(404);
} );
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'PageController@homepage');
Route::get('/workshop', 'PageController@workshop')->name('workshop');
Route::get('/education', 'PageController@education')->name('education');
Route::get('/training', 'PageController@training')->name('training');
Route::get('/resource', 'PageController@resource')->name('resource');
Route::get('/search', 'PageController@search')->name('search');
Route::get('/course/view/{id}', 'CourseController@view');
Route::get('/display/{item}/{file}', 'ImageController@display');

Route::group(['as' => 'email.'], function () {
    Route::post('/enquiry', 'EmailController@enquiry');
});

Route::group(['prefix' => 'admin', 'as' => 'admin', 'middleware' => 'auth'], function () {
//admin route
    Route::get('', 'CourseController@index')->name('admin');
    Route::get('password/change', 'Auth\ChangePasswordController@changePasswordView');
    Route::post('password/change', 'Auth\ChangePasswordController@changePassword');
    
    Route::group(['prefix' => 'course', 'as' => 'course'], function () {
        Route::get('index/{keyword?}', 'CourseController@index');
        Route::get('create', 'CourseController@create');
        Route::get('edit/{id}', 'CourseController@edit');
        Route::get('active/{id}', 'CourseController@active');
        Route::get('delete/{id}', 'CourseController@delete');
        Route::post('update/{id}', 'CourseController@update');
        Route::post('store', 'CourseController@store');
    });

    Route::group(['prefix' => 'trainer', 'as' => 'trainer'], function () {
        Route::get('index/{keyword?}', 'TrainerController@index');
        Route::get('create', 'TrainerController@create');
        Route::get('edit/{id}', 'TrainerController@edit');
        Route::get('active/{id}', 'TrainerController@active');
        Route::get('delete/{id}', 'TrainerController@delete');
        Route::post('update/{id}', 'TrainerController@update');
        Route::post('store', 'TrainerController@store');
    });

    Route::group(['prefix' => 'provider', 'as' => 'provider'], function () {
        Route::get('list/{id}/{keyword?}', 'ProviderController@listing');
        Route::get('index/{keyword?}', 'ProviderController@index');
        Route::get('create', 'ProviderController@create');
        Route::get('edit/{id}', 'ProviderController@edit');
        Route::get('confirm/{id}', 'ProviderController@confirm');
        Route::get('active/{id}', 'ProviderController@active');
        Route::get('delete/{id}', 'ProviderController@delete');
        Route::post('update/{id}', 'ProviderController@update');
        Route::post('store', 'ProviderController@store');
    });

    Route::group(['prefix' => 'banner', 'as' => 'banner'], function () {
        Route::get('index', 'BannerController@index');
        Route::get('create', 'BannerController@create');
        Route::get('edit/{id}', 'BannerController@edit');
        Route::get('active/{id}', 'BannerController@active');
        Route::get('delete/{id}', 'BannerController@delete');
        Route::post('update/{id}', 'BannerController@update');
        Route::post('store', 'BannerController@store');
    });
    
    //Enquiry 
    Route::get('/report/enquiry', 'ReportController@enquiry');
    
    //image
    Route::post('/image/edit', 'ImageController@edit')->name('upload');
});


#redirect /vst
Route::get('vst', function() {
    return redirect('course/view/27');
} );
#end /vst