let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
    'resources/assets/js/imagesloaded.pkgd.min.js',
    'resources/assets/js/isotope.pkgd.min.js',
    'resources/assets/js/select2.min.js', 
    'resources/assets/js/owl.carousel.min.js'],
    'public/js/all.js');
    
mix.styles([
    'node_modules/owl.carousel/dist/assets/owl.carousel.min.css',
    'node_modules/owl.carousel/dist/assets/owl.theme.default.min.css'
], 'public/css/carousel-main.css');
